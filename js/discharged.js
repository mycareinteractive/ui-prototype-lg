var Discharged = View.extend({

    id: 'discharged',
    
    template: 'discharged.html',
    
    css: 'discharged.css',
    
    trackOptions: {'nonInteraction': true, 'sessionControl': 'end'},
    
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/

    navigate: function(key) {
        if(key == 'CLOSE' || key == 'CLOSEALL' || key == 'MENU' || key == 'HOME') {
            //discharged page never closes
            return true;
        }
        else if(key == 'POWR') {
            return true;
        }
        return false;
    },
    
    click: function($jqobj) {
        return false;
    },
    
    uninit: function() {
        $.doTimeout('discharged clock');    // stop clock
    },
    
    renderData: function() {
        var context = this;
        var room = window.settings.room.toUpperCase();
        this.$('#room').text(room);
        this.$('#phone').text(window.settings.phone);
        
        // start clock
        $.doTimeout('discharged clock', 60000, function() {
            var d = new Date();
            var format = 'h:MM TT dddd, mmmm d, yyyy';
            var locale = window.settings.language;
            context.$('#datetime').text(d.format(format, false, locale));
            return true;
        });
        $.doTimeout('discharged clock', true); // do it now
        
        this.$('#revision').html('UpCare ' + window.settings.revision + '<br>' + window.settings.version + ' ' + window.settings.platformVersion);
    },

    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/

    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/
    
});    