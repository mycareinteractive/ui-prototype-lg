var Movies = View.extend({

    id: 'movies',
    
    template: 'movies.html',
    
    css: 'movies.css',
    
    pageSize: 6,
    
    subPageSize: 5,
    
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/

    navigate: function(key) {
    	if(key == 'UP' || key == 'DOWN') {
        	this.changeFocus(key, '#selections', 'a', '.selected');
            return true;
        }
        else if(key == 'LEFT' || key == 'RIGHT') {
            this.changeFocus(key, '#subselections');
            return true;
        }
        return false;
    },
    
    // When "ENTER" is pressed on a link, or when link is clicked by mouse or touch screen
    click: function($jqobj) {
        var linkid = $jqobj.attr('id');
        var parentid = $jqobj.parent().attr('id');
        var type = $jqobj.attr('data-type');
        var itemData = this._getMenuItemData($jqobj);
            
        if($jqobj.hasClass('back')) { // back button
            this.destroy();
            return true;
        }
        
        // folder page up/down
        if(linkid == 'next' || linkid == 'prev') {
            if (linkid=='next')
               this.scrollNext('#selections', 'vertical');
            else
               this.scrollPrev('#selections', 'vertical');
            this.$('#selections a.selected').removeClass('selected');
            this.$('#selection').hide();
            this.pagination('#selections', '#prev', '#next', 'vertical');
            return true;
        }
        
        // asset page up/down
        if(linkid == 'subnext' || linkid == 'subprev') {
            if(linkid=='subnext')
                this.focusNextPage('#subselections', this.subPageSize, this.assets.ListOfEntry.length, 'horizontal');
            else
                this.focusPrevPage('#subselections', this.subPageSize, this.assets.ListOfEntry.length, 'horizontal');
            return true;
        }
        
        /*
        // selecting a folder
        if($jqobj.hasClass('folder') && itemData) {
            this.$('#selections a.selected').removeClass('selected');
            $jqobj.addClass('selected');
            this._buildAssets(itemData.tagAttribute.hierarchyUID);
            this.blur(this.$('#subselections .asset:first'));
            this.focus(this.$('#subselections .asset:first'));
            return true;
        }
        */
        
        // selecting a video
        if($jqobj.hasClass('playbutton')||$jqobj.hasClass('asset')) {
            var $asset = $jqobj.hasClass('playbutton')?$jqobj.parent():$jqobj;
            itemData = this._getMenuItemData($asset);
            var breadcrumb = this.$('#label p').text();
            var label = this.$('#selections a.selected').text();
            var page = new MovieDetail({parent:this, breadcrumb:breadcrumb, label:label, data: itemData});
            page.render();
            return true;
        }
        
        return false;
    },
    
    focus: function($jqobj) {
        if($jqobj.hasClass('asset')) {
            this._super($jqobj);
            this._updateTitle($jqobj);
            this.pagination('#subselections', '#subprev', '#subnext', 'horizontal');
        }
        else if($jqobj.hasClass('folder')) {
        	var itemData = this._getMenuItemData($jqobj);
        	this.$('#selections a.selected').removeClass('selected');
            $jqobj.addClass('selected');
            this.pagination('#selections', '#prev', '#next', 'vertical');
            this._buildAssets(itemData.tagAttribute.hierarchyUID);
            this.blur(this.$('#subselections .asset:first'));
            this.focus(this.$('#subselections .asset:first'));
        }
    },
    
    blur: function($jqobj) {
    	if($jqobj.hasClass('folder')) {
    		this.$('.folder').removeClass('selected');
    	}
    	else if(!$jqobj.hasClass('pagebutton')) {
            this._super($jqobj);
        }
        this._updateTitle('');
    },
    
    renderData: function() {
        $('<p></p>').html(this.data.label).appendTo(this.$('#label'));
        $('<p></p>').html(this.breadcrumb).appendTo(this.$('#breadcrumb'));
        
        this.label = this.data.label;
        
        var context = this;
        this.$el.delegate('span.playbutton', 'click', function(e){
            context.click($(this));
            e.preventDefault();
        });
        
        this._buildCategories();
    },
    
    shown: function() {
    	this.$('#selections').show();
        this.$('#selections .folder:first').click();
    },
    
    refresh: function() {
        var $folder = this.$('#selections a.selected');
        var itemData = this._getMenuItemData($folder);
        if(itemData && itemData.tagAttribute)
            this._buildAssets(itemData.tagAttribute.hierarchyUID);
    },
    
    uninit: function() {
        this.$el.undelegate('span.playbutton', 'click');
    },
    
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
    
    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/
    _buildCategories: function () {
        var context = this;
        context.$('#selections').empty();
        
        var ewf = ewfObject();
        var folders = this.folders = getListEntry(ewf.movies).DataArea;
        
        if(folders && $.isArray(folders.ListOfEntry)) {
            $.each(folders.ListOfEntry, function(i, entry){
                if(entry.tagName == 'Folder') {
                    $('<a href="#" class="folder ellipsis"></a>').attr('data-index', i).attr('title', entry.tagAttribute.entryName)
                        .text(entry.tagAttribute.entryName).appendTo(context.$('#selections'));
                }
            });
            
            // pad some extra lines to fill the last page
            var padNum = this.pageSize - (folders.ListOfEntry.length % this.pageSize);
            if(padNum == 6) padNum = 0;
            for(var i=0; i<padNum; i++) {
                $('<div class="padding"></div>').appendTo(context.$('#selections'));
            }
        }
        
    },
    
    _buildAssets: function (huid) {
        var context = this;
        context.$('#selection #subselections').empty();
        
        context.$('#subprev').hide();
        context.$('#subnext').hide();
        
        var assets = this.assets = getListEntry(huid).DataArea;
        
        if(assets && $.isArray(assets.ListOfEntry)) {
            $.each(assets.ListOfEntry, function(i, entry){
                if(entry.tagName == 'Asset') {
                    var isBookmarked = (entry.tagAttribute.ticketIDList && entry.tagAttribute.ticketIDList != '');
                    var $entry = $('<a href="#" class="asset padding"></a>').attr('data-index', i).attr('title', entry.ListOfMetaData.Title);
                    var poster = entry.ListOfMetaData.PosterBoard;
                    poster = poster.split(',')[0];
                    $('<img></img>').attr('src', poster).appendTo($entry);
                    $('<span class="playbutton"></span>').appendTo($entry);
                    $entry.appendTo(context.$('#selection #subselections'));
                }
            });
            
            // pad some extra lines to fill the last page
            var padNum = this.subPageSize - (assets.ListOfEntry.length % this.subPageSize);
            if(padNum == 6) padNum = 0;
            for(var i=0; i<padNum; i++) {
                $('<div class="padding"></div>').appendTo(context.$('#selection #subselections'));
            }
        }
        
        this.$('#selection').show();
    },
    
    _updateTitle: function(val) {
        var title = '';
        if(!val)
            title = val;
        else if(val instanceof jQuery)
            title = val.attr('title');
            
        this.$('#selection #title').text(title);
    },
    
    _getMenuItemData: function ($obj) {
        var itemData = null;
        var itemIndex = $obj.attr('data-index');
        var entries = null;
        
        if($obj.hasClass('folder'))
            entries = this.folders;
        else if($obj.hasClass('asset'))
            entries = this.assets;
            
        if(itemIndex && entries && entries.ListOfEntry) {
            itemData = entries.ListOfEntry[itemIndex];
        }
        
        return itemData;
    }
});    