var MyPrograms = View.extend({

    id: 'myprograms',
    
    template: 'myprograms.html',
    
    css: 'myprograms.css',
    
    pageSize: 6,
    
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/

    navigate: function(key)	{
    	if(key == 'UP' || key == 'DOWN') {
    		this.changeFocus(key, '#selections');	
    		return true;
    	}
    	return false;
    },
    
    // When "ENTER" is pressed on a link, or when link is clicked by mouse or touch screen
    click: function($jqobj) {
    	var linkid = $jqobj.attr('id');
        var parentid = $jqobj.parent().attr('id');
        var type = $jqobj.attr('data-type');
        var itemData = this._getMenuItemData($jqobj);
        	
    	if($jqobj.hasClass('back')) { // back button
    		this.destroy();
    		return true;
    	} 
    	
    	// page up/down
    	if( linkid == 'next' || linkid == 'prev') {
    	    if (linkid=='next')
               this.scrollNext('#selections', 'vertical');
            else
               this.scrollPrev('#selections', 'vertical');
    	    this.pagination('#selections', '#prev', '#next', 'vertical');
    	    return true;
    	}
    	
    	// selecting a video
    	if($jqobj.hasClass('asset') && itemData) {
    	    var breadcrumb = this.$('#label p').text();
    	    var page = new ProgramDetail({className:'', parent:this, breadcrumb:breadcrumb, data: itemData});
    	    page.render();
    	    return true;
    	}
    	
    	return false;
    },
    
    focus: function($jqobj) {
        if($jqobj.parent().attr('id') == 'selections') {
            this._super($jqobj);
            this.pagination('#selections', '#prev', '#next', 'vertical');
    	}
    },
    
    blur: function($jqobj) {
    	this._super($jqobj);
    },
    
    renderData: function() {
        $('<p></p>').html(this.data.label).appendTo(this.$('#label'));
        $('<p></p>').html(this.breadcrumb).appendTo(this.$('#breadcrumb'));
        
        this._buildBookmarks();
    },
    
    shown: function() {
        this.blur(this.$('#selections .asset:first'));
        this.focus(this.$('#selections .asset:first'));
    },
    
    refresh: function() {
        this._buildBookmarks();
    },
    
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
    
        /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/
    _buildBookmarks: function () {
        var context = this;
        context.$('#selections').empty();
        
        var ewf = ewfObject();
        var poUid = ewf.healthcarePOUID;
        
        var tickets = getListTicket();
        
        if(!tickets || !tickets.DataArea || !$.isArray(tickets.DataArea.ListOfTicket))
            return;
        
        var bookmarks = tickets.DataArea.ListOfTicket;
        // filter only assets under healthcare PO
        bookmarks = $.grep(bookmarks, function(ticket, i){
            var po = $.grep(ticket.ListOfSubEntry, function(sub, j) {
                return sub.tagName == 'ProductOffering';
            });
            return (po && po[0] && po[0].tagAttribute && po[0].tagAttribute.productOfferingID==poUid);
        });
        
        // build up the title list
        this.bookmarks = bookmarks;
        $.each(bookmarks, function(i, ticket){
            $.each(ticket.ListOfSubEntry, function(j, sub){
                if(sub.tagName == 'Asset') {
                    $('<a href="#" class="asset ellipsis"></a>').attr('data-index', i).attr('title', sub.ListOfMetaData.Title)
                        .text(sub.ListOfMetaData.Title).appendTo(context.$('#selections'));
                    return false;
                }
            });
        });
            
        // pad some extra lines to fill the last page
        var padNum = this.pageSize - (bookmarks.length % this.pageSize);
        if(padNum == 6) padNum = 0;
        for(var i=0; i<padNum; i++) {
            $('<div class="padding"></div>').appendTo(context.$('#selections'));
        }
    },
    
    _getMenuItemData: function ($obj) {
        var itemData = null;
        var itemIndex = $obj.attr('data-index');
        
        if(itemIndex && this.bookmarks && this.bookmarks) {
            itemData = this.bookmarks[itemIndex];
        }
        
        return itemData;
    }
});    