var dineintro = View.extend({
    
    id: 'dineintro',
    
    template: 'dineintro.html',
    
    css: 'dineintro.css',
    
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/
    navigate: function(key)    {
        var navkeys = ['LEFT','RIGHT'];
        if(navkeys.indexOf(key)!=-1) {
            this.changeFocus(key);
            return true;
        }
        
        return false;
    },
    
    click: function($jqobj) {
	        var dataobj = {};

			var currLabel = this.data.label
			var linkid = $jqobj.attr('id');
        // Get Patient MRN
		var mrn = this.mrn;
		if(!mrn)
			mrn = getMRNDATA();		
		this.mrn = mrn;
		
		dataobj['room'] = window.settings.room;
        dataobj['bed'] = window.settings.bed;
        dataobj['name'] = window.settings.userFullName;
        var preferredname = '';
		window.settings.mrn = mrn;
		window.settings.vendor = 'Kaiser';
		
		
        if(linkid == 'back' ) {

            this.destroy();
            return true;
        }
        else if(linkid == 'decline') {
    	       var data = {
    	           text1: '<p>Hungry?<br/><br/><br/></p><p><span>Please press the red call <br/>button and a member of your <br/>Care Team will be by shortly</span></p>',    	         
    	       };
			   this.destroy();
    	       var page = new Information({className:'dinedecline', data: data});
    	       page.render();
				
				return true;
		}
		else if(linkid == 'confirm') {

		    dataobj = {};
			dataobj['mrn'] = window.settings.mrn;
			dataobj['vendor'] = window.settings.vendor;

			var url = "http://localhost:9080/GetMeals"			
			
			var xml  = getXdXML(url, dataobj);

			var MealsResult = '';
			$(xml).find("GetMealsResponse").each(function() {
				MealsResult = $(xml).find("GetMealsResult").text();			
			});
							

            // Or a speical link in the content that triggers next page
            var page = new dinediet({viewId: 'dinediet', breadcrumb:currLabel, data: xml});
            page.render();
            return true;
        }
        
    
        
        return false;
    },
    
    renderData: function() {

		var context = this;
				
		this.$('#patient').html('<p>' + this.data.userName + ' - Room ' + this.data.roomNumber + '</p>');

    },
	
    
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
    
    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/

});

