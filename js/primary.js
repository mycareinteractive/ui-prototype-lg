var Primary = View.extend({
    
    id: 'primary',
    
    template: 'primary.html',
    
    css: null, // we preload css for primary to eliminate the paint effect
    
    className: 'home',
    
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/
    navigate: function(key)    {

        Debug.log('REPORTED VERSION: ' + window.settings.version);

        if(key == 'CLOSE' || key == 'CLOSEALL' || key == 'MENU' || key == 'HOME' || key == 'EXIT') {
            //primary page never closes
        	if(window.settings.version == 'SAMSUNGTV') {
        		var widgetAPI = new Common.API.Widget();
        		widgetAPI.blockNavigation(event);
        	}
            else if(window.settings.version == "TEST" && key == "EXIT"){
                Debug.log('SETTING THE OSD TO OFF. MODE BEING SET TO: ' + hcap.mode.HCAP_MODE_0);

                hcap.mode.setHcapMode({
                    mode: hcap.mode.HCAP_MODE_0,
                    onSuccess: function(){
                        Debug.log('Hiding the OSD');
                    },
                    onFailure: function(f){
                        Debug.log('We were not able to hide the OSD. Returned with error: \n' + f.errorMessage);
                    }
                });

                Debug.log('SETTING THE OSD TO OFF -- EXECUTED CALL');

                Debug.log('SETTING THE OSD TO OFF --- FINSISHED PROCESSING');
            }

            return true;
        }
        else if(key == 'POWR') {
            return true;
        }
        else if(key == 'UP' || key == 'DOWN') {
        	this.changeFocus(key, this.$('#menus .menu').filter(':visible'));
            return true;
        }
        else if(key == 'LEFT' || key == 'RIGHT') {
            var $menu = this.$('#menubar a.selected');
            if($menu.length) {
            	$menu = (key == 'LEFT')? $menu.prev(): $menu.next();
            }
            else {
            	$menu = this.$('#menubar a').first();
            }
            this.blur($menu);
            this.focus($menu);
            return true;
        }
        // Voice recognition below
        else if(key == 'CAREBOARD') {
        	this.$('#menus #ecareboard').click();
        	return true;
        }
        else if(key == 'KAISER') {
        	this.$('#menus #kporg').click();
        	return true;
        }
        else if(key == 'PAIN CONTROL') {
        	this.$('#menus #thrive #paincontrol').click();
        	return true;
        }
        else if(key == 'THRIVE' || key == 'LEARN' || key == 'VISIT' || key == 'FUN' || key == 'RELAX') {
        	var linkid = key.toLowerCase();
        	this.$('#menubar #'+linkid).click();
        	return true;
        }
        else if(key == 'YELLOW')
        {
            // RF channel class 3 change request
            hcap.channel.requestChangeCurrentChannel({
                "channelType":hcap.channel.ChannelType.RF,
                //"majorNumber":75,
                //"minorNumber":1,
                //"rfBroadcastType":hcap.channel.RfBroadcastType.CABLE,
                "programNumber": 1,
                "frequency": 321000000,
                "rfBroadcastType":hcap.channel.RfBroadcastType.TERRESTRIAL,
                "onSuccess":function() {
                    $('body').hide();
                },
                "onFailure":function(f) {
                    Debug.log("onFailure : errorMessage = " + f.errorMessage);
                }
            });
        }
        
        var words = key.replace(/\s/g, '').toLowerCase();
        var $link = this.$('#menus .menu').filter(':visible').find('#'+words);
        if($link.length) {
        	$link.click();
        	return true;
        }
        return false;  
    },
    
    click: function($jqobj) {
        var ret = false;
        var context = this;
        
        var linkid = $jqobj.attr('id');
        // stupid legacy codde
        $("#K_submenu").removeClass().addClass(linkid);
        $("#K_submenu").text(linkid);
        
        if($jqobj.parent().attr('id')=='buttons') { // side bar
            this.QuickLaunch(linkid);
            ret = true;
        }
        else if($jqobj.parent().parent().attr('id')=='menus') { // menu items
            var parent = $jqobj.parent().attr('title');
            var pagePath = this.pagePath + '/' + $jqobj.parent().attr('id') + '/' + linkid;
            var type = $jqobj.attr('data-type');
            var itemData = this._getMenuItemData($jqobj);
            msg(itemData);
            
            switch(type) {
                case 'link':    // website
                    this.openWebpage(linkid, parent, itemData, pagePath);
                    break;
                case 'menu':    // next level menu
                    this.openSecondary(linkid, parent, itemData, pagePath);
                    break;
                case 'grid':    // guide
                    this.openGuide(linkid, parent, itemData, pagePath);
                    break;
                case 'browser': // web browser
                    this.openBrowser(linkid, parent, itemData, pagePath);
                    break;
                
            }
            
            switch(linkid) {
                case 'movies':    // movie
                    this.openMovies(linkid, parent, itemData, pagePath);
                    break;
                case 'ecareboard':   //careboard
                    this.openCareboard(linkid, parent, itemData, pagePath);
                    break;
                case 'myprograms':
                    this.openMyPrograms(linkid, parent, itemData, pagePath);
                    break;
                case 'allprograms':
                    this.openAllPrograms(linkid, parent, itemData, pagePath);
                    break;
                case 'scenictv':
                    this.openVideoPlayer2(linkid, parent, itemData, pagePath);
                    break;
                case 'mymenu':
                    this.openDine(linkid, parent, itemData, pagePath);
                    break;
                case 'paincontrol':
                	(new PainControl({className:linkid, breadcrumb:parent, data: itemData, pagePath: pagePath,
                        oncreate: function() { context.updateNavbar(); }, 
                        ondestroy: function() { context.updateNavbar(); }
                    })).render();
                    break;

            }
            
            ret = true;
        }
        else if($jqobj.parent().parent().attr('id')=='menubar') { // menu bar drop down click
            ret = true;
        }
        
        if(linkid == 'nightmode') {
            $('#nightmode-overlay').toggle();
            if($('#nightmode-overlay').is(':visible'))
                this.$('#nightmode').addClass('on');
            else
                this.$('#nightmode').removeClass('on');
        }
        else if(linkid.indexOf('theme-') == 0) {
        	$('body').removeClass('theme-spring theme-summer theme-fall theme-winter').addClass(linkid);
        }
        
        return ret;
    },
    
    
    focus: function($jqobj) {
        if($jqobj.parent().parent().attr('id')=='menubar') {
        	this.$('#menubar a.selected').removeClass('selected');
            this.$('#navigation #menus div.menu').hide();
            $jqobj.addClass('selected');
            var menuid = $jqobj.attr('id');
            //this.$('#navigation #menus #'+menuid).show();
            this.$('#navigation #menus #'+menuid).slideDown(200);
        }
        else {
        	this._super($jqobj);
        }
    },
    
    blur: function($jqobj) {
        this._super($jqobj);
    },
    
    renderData: function() {
    
        this._buildPage();
        this._buildMenu();


        this.$el.show();
        
        var context = this;

        // start clock
        $.doTimeout('primary clock', 60000, function() {
            var d = new Date();
            var format = 'h:MM TT dddd, mmmm d, yyyy';
            var locale = window.settings.language;
            context.$('#header #datetime').text(d.format(format, false, locale));
            return true;
        });
        $.doTimeout('primary clock', true); // do it now
        
        // start slideshow
        this.$('#slideshow img:gt(0)').hide();
        $.doTimeout('primary slideshow', 5000, function() {
            context.$('#slideshow :first-child').fadeOut(1200)
                .next('img').fadeIn(1200)
                .end().appendTo(context.$('#slideshow'));
            return true;
        });
        
        this.updateNavbar();
        
        this.$('#revision').html('UpCare ' + window.settings.revision + '<br>' + window.settings.version + ' ' + window.settings.platformVersion);
    },
    
    uninit: function() {
        $.doTimeout('primary clock'); // stop clock
        $.doTimeout('primary slideshow'); // stop slideshow
    },
    
    refresh: function() {
        this._buildPage();
        this._buildMenu();
        $.doTimeout('primary clock', true);
    },
    
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
    QuickLaunch: function(linkid) {
    	if(linkid=='nav-home' && window.pages.topPage() && window.pages.topPage().viewId != this.viewId) { 
            // clicked HOME button to return to home, need to register a hit
            this._track();
        }
        
        keypressed('CLOSEALL'); //force going back to main menu
        //keypressed(216);    
        
        switch(linkid) {
            case 'nav-home':
                break;
            case 'nav-watchtv':
                this.open('tv');
                break;
            case 'nav-careboard':
                this.open('ecareboard');
                break;
            case 'nav-leafmail':
                this.openLeafMail();
                break;
            case 'nav-languages':
                this.openLanguages();        
                break;
            case 'nav-internet':
                this.open('kporg');
                break;
            case 'nav-phone':
                this.openComingSoon();
                break;
            
        }
    },
    
    updateNavbar: function() {
        var locale = window.settings.language;
        var $obj = this.$('#menus');
        $obj.removeAttr('class');                                    
		$obj.addClass(locale);

        if(window.pages.topPage() && window.pages.topPage().viewId != this.viewId) { // show home icon
            $('#sidebar #nav-languages').hide();
            $('#sidebar #nav-home').show();
        }
        else {
            $('#sidebar #nav-languages').show();
            $('#sidebar #nav-home').hide();
            $('#sidebar a.active').removeClass('active');
        }
    },
    
    open: function(tagName) {
        var $obj = this.$('#menus a#' + tagName);
        if($obj.length == 1) {
            this.click($obj);
        }
    },
    
    openSecondary: function(linkid, breadcrumb, data, pagePath) {
        var context = this;
        var page = new Secondary({className:linkid, breadcrumb:breadcrumb, data: data, pagePath: pagePath,
            oncreate: function() {
                context.updateNavbar();
            }, 
            ondestroy: function() {
                context.updateNavbar();
            }
        });
        page.render();
    },
   
    openDine: function(linkid, breadcrumb, data, pagePath) {
        var data    = getUserDataDATA();
        var context = this;
        var page = new dineintro({className:linkid, breadcrumb:breadcrumb, data: data, pagePath: pagePath,
            oncreate: function() {
                context.updateNavbar();
            }, 
            ondestroy: function() {
                context.updateNavbar();
            }
        });
        page.render();
    },
   
    openWebpage: function(linkid, breadcrumb, data, pagePath) {
        var context = this;
        var page = new WebSite({className:linkid, breadcrumb:breadcrumb, data: data, pagePath: pagePath,
            oncreate: function() {
                context.updateNavbar();
            }, 
            ondestroy: function() {
                context.updateNavbar();
            }
        });
        page.render();
    },
    
    openBrowser: function(linkid, breadcrumb, data, pagePath) {
        var context = this;
        var page = new WebBrowser({className:linkid, breadcrumb:breadcrumb, data: data, pagePath: pagePath,
            oncreate: function() {
                context.updateNavbar();
            }, 
            ondestroy: function() {
                context.updateNavbar();
            }
        });
        page.render();
    },
    
    openKPorg: function(linkid, breadcrumb, webpage, pagePath) {
        var context = this;
        var page = new WebSite({className:'kporg', label:'kp.org', breadcrumb:breadcrumb, webpage:webpage, pagePath: pagePath,
            oncreate: function() {
                context.updateNavbar();
            }, 
            ondestroy: function() {
                context.updateNavbar();
            }
        });
        page.render();
    },
    openGuide: function(linkid, breadcrumb, data, pagePath) {
        var context = this;
        var page = new TVGuide({className:linkid, breadcrumb:breadcrumb, data: data, pagePath: pagePath,
            oncreate: function() {
                context.updateNavbar();
            }, 
            ondestroy: function() {
                context.updateNavbar();
            }
        });
        page.render();
    },
    
    openMovies: function(linkid, breadcrumb, data, pagePath) {
        var context = this;
        var page = new Movies({className:linkid, breadcrumb:breadcrumb, data: data, pagePath: pagePath,
            oncreate: function() {
                context.updateNavbar();
            }, 
            ondestroy: function() {
                context.updateNavbar();
            }
        });
        page.render();
    },
    
    openMyPrograms: function(linkid, breadcrumb, data, pagePath) {
        var context = this;
        var page = new MyPrograms({className:linkid, breadcrumb:breadcrumb, data: data, pagePath: pagePath,
            oncreate: function() {
                context.updateNavbar();
            }, 
            ondestroy: function() {
                context.updateNavbar();
            }
        });
        page.render();
    },
    
    openAllPrograms: function(linkid, breadcrumb, data, pagePath) {
        var context = this;
        var page = new AllPrograms({className:linkid, breadcrumb:breadcrumb, data: data, pagePath: pagePath,
            oncreate: function() {
                context.updateNavbar();
            }, 
            ondestroy: function() {
                context.updateNavbar();
            }
        });
        page.render();
    },

    openCareboard: function(linkid, breadcrumb, data, pagePath) {
        var context = this;
        var page = new Careboard({className:linkid, breadcrumb:breadcrumb, data: data, pagePath: pagePath,
            oncreate: function() {
                context.updateNavbar();
            }, 
            ondestroy: function() {
                context.updateNavbar();
            }
        });
        page.render();
    },
    
    openLeafMail: function(linkid, breadcrumb, data, pagePath) {
        var context = this;
        var page = new LeafMail({className:linkid, breadcrumb:breadcrumb, data: data, pagePath: pagePath,
            oncreate: function() {
                context.updateNavbar();
            }, 
            ondestroy: function() {
                context.updateNavbar();
            }
        });
        page.render();
    },
    
    openComingSoon: function(linkid, breadcrumb, data, pagePath) {
        var context = this;
        var page = new Dialog({className:linkid, breadcrumb:breadcrumb, data: data, message:'Coming Soon...', pagePath: pagePath,
            oncreate: function() {
                context.updateNavbar();
            }, 
            ondestroy: function() {
                context.updateNavbar();
            }
        });
        page.render();
    },
    
    openVideoPlayer2: function(linkid, breadcrumb, data, pagePath) {
        var context = this;
        var type, url, display;
        if(data && data.attributes && data.attributes.attribute && data.attributes.attribute.text) {
            var params = data.attributes.attribute.text.split(',');
            type = params[0];
            url = params[1];
            display = params[2] || '';
        
            var page = new VideoPlayer2({className:linkid, breadcrumb:breadcrumb, data: data, type: type, url: url, display: display, pagePath: pagePath,
                oncreate: function() {
                    context.updateNavbar();
                }, 
                ondestroy: function() {
                    context.updateNavbar();
                }
            });
            page.render();
        }
    },
    
    openLanguages: function(linkid, breadcrumb, data, pagePath) {
        var context = this;
        var oldLang = window.settings.language;
        var page = new Languages({className:linkid, breadcrumb:breadcrumb, data: data, pagePath: pagePath,
            oncreate: function() {
                context.updateNavbar();
            }, 
            ondestroy: function() {
                context.updateNavbar();
                var newLang = window.settings.language;
                // when closed, determine if we need to translate primary page.
                if(newLang != oldLang) {
                    context.translate(newLang);
                    context.refresh();
                }
            }
        });
        page.render();
    },
    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/
    
    _buildPage: function() {
    
        var patientDATA = loadJSON('patient');
        var patientstatus   = patientDATA.status;      
        var patientname = patientDATA.userFullName;
        var firstnames = ""; // <- forgot to initialize to an empty string
        if (typeof patientname != 'undefined') {
            var splitName = patientname.split(" ");
            var surname = splitName[splitName.length-1]; //The last one
            for (var i=0; i < splitName.length-1; i++){
                firstnames += splitName[i] + " ";
            }
        }
        var room = patientDATA.roomNumber.toUpperCase();
        this.$('#header #patient span#name').text(firstnames);
        this.$('#header #patient span#room').text(room);
        this.$('#header #patient span#tel').text(patientDATA.phoneNumber);
    },
    
    _buildMenu: function() {
        this.$('#navigation #menubar').empty();
        this.$('#navigation #menus').empty();
        
        getMenuXML();
        if($("#XMLCALL_error").text().indexOf("ERROR") >= 0){
            msg('### Unable to get MENU data!!');
            this.$('#navigation #menubar').text('Oops! Unable to get MENU data!!');
            return;
        }
        
        this.data = getJsonMenuFromXML().response.menuBar;
        var menuObj = this.data.menu;
        
        var $menu = $('<div id="menu"></div>');
        var $menutabs = $('<div id="menutabs"></div>');
        var $menus = $('<div id="menus"></div>');
        
        var context = this;
        $.each(menuObj, function(i,row){
            $('<a href="#"></a>').attr('id', row['tag']).text(row['label']).appendTo($menu);
            $('<div class="menutab"></div>').attr('id', row['tag']).text(row['label']).appendTo($menutabs);
            var $submenu = context._buildSubMenu(row, row["tag"]);
            $('<div class="menu"></div>').attr('id',row['tag']).attr('title', row['label']).attr('audio',row['image']||'').attr('data-index',i).append($submenu.html()).appendTo($menus);
        });
       
        this.$('#navigation #menubar').html($menu);
        this.$('#navigation #menus').html($menus.html());
    },
    
    _buildSubMenu: function (menu, tagName)   {
        var $submenu = $('<div></div>');
        var items = menu[tagName];
        if(items.label) { // single object
            var item = items;
            $('<a href="#"></a>').attr('id', item.tag).attr('audio', item.image||'').attr('title', item.label).attr('data-type', item.type).attr('data-index',-1).text(item.label).appendTo($submenu);
        } 
        else { // array
            $.each(items, function(i,item){          
                $('<a href="#"></a>').attr('id', item.tag).attr('audio', item.image||'').attr('title', item.label).attr('data-type', item.type).attr('data-index',i).text(item.label).appendTo($submenu);
            });
        }
        
        return $submenu;
    },
    
    _getMenuItemData: function ($obj) {
        var itemData = null;
        var itemTag = $obj.attr('id');
        var parentTag = $obj.parent().attr('id');
        
        $.each(this.data.menu, function(i,menu){
            if(menu.tag == parentTag) {
                var items = menu[menu.tag];
                if($.isArray(items)){ // an array of items
                    
                    $.each(items, function(i,item){
                        if(item.tag == itemTag) {
                            itemData = item;
                            return false;   // break
                        }
                    });
                    
                }
                else { // only one item
                    if(items.tag == itemTag)
                        itemData = items;
                }
                return false; // break
            }
        });
        
        return itemData;
    }

});