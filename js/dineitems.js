var dineitems = View.extend({

    id: 'dineitems',
    
    template: 'dineitems.html',
   
    css: 'dineitems.css',
    
    pageSize: 8,

	course: null,
    
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/

    navigate: function(key)	{
    	if(key == 'UP' || key == 'DOWN') {
    		this.changeFocus(key, '#selections');	
    		return true;
    	}
    	return false;
    },
    
    // When "ENTER" is pressed on a link, or when link is clicked by mouse or touch screen
    click: function($jqobj) {
		var currLabel = this.data.label;
    	var linkid = $jqobj.attr('id');
        var parentid = $jqobj.parent().attr('id');
        var type = $jqobj.attr('data-type');
		var currlink = this.currsel;
		var keyname = $jqobj.attr('keyname');
		
		//var itemData = this._getMenuItemData($jqobj);
        	
    	if($jqobj.hasClass('back')) { // back button
    		this.destroy();
    		return true;
    	} 
    	
		if(linkid == 'exit') {			


			this.destroy();
            return false;
        }

    	// page up/down
    	if( linkid == 'next' || linkid == 'prev') {
    	    var pos = this.$('#selections').scrollTop();
    	    var height = this.$('#selections').height();
    	    pos = (linkid=='next')? pos+height: pos-height;
    	    this.$('#selections').scrollTop(pos);
    	    return true;
    	}
		if(linkid == 'view') { // view order	
			if(window.meals.length>=1) {				
                var page = new dineview({viewId: 'dineview', breadcrumb:currLabel, data: ''});
                page.render();                
			} else {
            	var data = {
    	           text1: '<p>No Items Selected<br/><br/></p><p><span>Please make your selections<br/> and then view your order', 
    	       };
    	       var page = new Information({className:'dinedecline', data: data});
    	       page.render();
            }
			return true;
        }
		if(linkid == 'order') { // order item			
			var currsel = this.currsel;
			var currid = currsel.attr("data-index");
			var title = currsel.attr("title");
			var maxcarbs = this.maxcarbs;
			var carbs = currsel.attr("carbs");
			
			var totalcarbs = this.totalcarbs;
			var checkcarbs = Number(carbs) + Number(totalcarbs);
			msg(checkcarbs);

			this.coursemax = window.settings.coursemax;
			var coursemax = this.coursemax;
			var itemselected = currsel.hasClass('selected');
			if(itemselected!='')  {
				currsel.removeClass('selected')			
				this.selectedcount = this.selectedcount - 1;
				
				var items = window.meals;  
				var i = 0;		
				for(var i=0; i<items.length; i++) {
					var item = items[i];
					var id = item["id"];
					msg('id ' + id + ' currid ' + currid);
					if(id==currid)
						window.meals.splice(i,1);
				}		
                totalcarbs = totalcarbs - Number(carbs);				
				this.$('#nutrientstab #totalcarbs').html(totalcarbs);			
				this.totalcarbs = totalcarbs;
				window.settings.totalcarbs = totalcarbs;

		
			 } else {
                if(checkcarbs > maxcarbs) {
					var data = {
						text1: '<p>Warning<br/><br/><br/><span>You are over your carbohydrate limit.<br/><br/>Please select checked item and click "Remove Item" <br/>prior to selecting another</p>'
					};
					var page = new Information({className:'maxselected', data: data});
					page.render();	
					return true;
				}
             
				if(this.selectedcount>= coursemax) {
					var data = {
						text1: '<p>Warning<br/><br/><br/><span>You are over your limit <br/>for this course.<br/><br/>Please select checked item and click "Remove Item" <br/>prior to selecting another</p>'
					};
					var page = new Information({className:'maxselected', data: data});
					page.render();	
					return true;
				} else {				
					this.selectedcount = this.selectedcount + 1;
				}
				currsel.addClass('selected');	
				var item = [];
				item['id'] = currid;
				item['title'] = title;				
				item['qty'] = '1';
				item['carbs'] = carbs;
				item['max'] = coursemax;
				var len = window.meals.length;
				window.meals[len] = item;
				totalcarbs = totalcarbs + Number(carbs);				
				this.$('#nutrientstab #totalcarbs').html(totalcarbs);			
				this.totalcarbs = totalcarbs;
				window.settings.totalcarbs = totalcarbs;
				
			}
			this.$('#order').html("Order Item");
			
			
    	} 
    	
		if(linkid == 'help') {
    	       var data = {
    	           text1: '<p>Hungry?<br/><br/><br/></p><p><span>Please press the red call <br/>button and a member of your <br/>Care Team will be by shortly</span></p>',    	         
    	       };
    	       var page = new Information({className:'dinedecline', data: data});
    	       page.render();				
				return true;
		}

		if($jqobj.hasClass('item')) {
			this.currsel = this.$('#selections a.active');
			var itemselected = $jqobj.hasClass('selected')
			if(itemselected) 
				this.$('#order').html("Remove Item");
			else 
				this.$('#order').html("Order Item");
			var image = this.imageData[keyname];
			
			if(typeof image == 'undefined') {
				this.$('#body img.itemimage').attr('src','./images/dine/dining_image-not-available.jpg');
			 } else {
				this.$('#body img.itemimage').attr('src','./images/dine/'+image);				
			}
		}
		    	
    	return false;
    },
    
    focus: function($jqobj) {
		
        if($jqobj.parent().attr('id') == 'selections' || $jqobj.attr('id')=='order') {
            this._super($jqobj);
    	}
    },
    
    blur: function($jqobj) {
    	this._super($jqobj);
    },
    
    renderData: function() {		
		this.selectedcount = 0;
		this._getImageData();
        this._buildItems();
    },
    
    refresh: function() {
        this._buildItems();
    },
    
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
    
        /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/
    
	_getImageData: function() {
		var url = './Food_Images.xml';
        var imageData = getXMLFile(url);
		var parsedData = eval('('+imageData+')');
		this.imageData = parsedData.images;		
		return true;
	},
	
		
	_buildItems: function () {
		
		var context = this;
        context.$('#selections').empty();
        var data = this.data;
		
		var meals = Array();
		var itemstring = '';
		var mealname = '';
		var maxcarbs = window.settings.maxcarbs;
		this.maxcarbs = maxcarbs;
		var totalcarbs = Number(window.settings.totalcarbs);
		var selectedcount = 0;
		this.totalcarbs = totalcarbs;
		this.$('#nutrientstab #totalcarbs').html(totalcarbs);			
		var coursename = $(data).attr("Name");
		
		this.$('#banner #course').html(coursename);
		
		if(maxcarbs>=1) {			
			this.$('#nutrientstab').attr('style','display:block');			
			this.$('#nutrientstab #max').html(maxcarbs);			
		} else 
			this.$('#nutrientstab').attr('style','display:none');		
        var cnt = 0;
		$(data).find("Item").each(function() { 

				var item = $(this).attr("TrayTicketLongName");
				var id = $(this).attr("Id");
				var keyname = $(this).attr("Keyname");
				
				var carbs = '';
				
				subdata = $(this);				
				if(maxcarbs>=1) {
					var carbs = 0;
					$(subdata).find("ItemNutrient").each(function() { 
						var nutrientid = $(this).attr("NutrientId");						
						if(nutrientid=='10101') {
							carbs = $(this).text();
						}
					});
				}
				
				var $entry = $('<a href="#" class="item ellipsis"></a>').attr('data-index', id).attr('title', item)
					.attr('carbs',carbs).attr('keyname',keyname).text(item);
				if(maxcarbs>=1) 
					$('<span style="float:right"></span>').text(carbs).appendTo($entry);

				$entry.appendTo(context.$('#selections'));
				
				var items = window.meals;  
				var i = 0;		
                
				for(var i=0; i<items.length; i++) {
					var item = items[i];
					var itemid = item["id"];					
					 if(itemid==id) {
						$entry.addClass('selected');
                        selectedcount = selectedcount + 1;
                       }
                       
		
				}		
                cnt = cnt + 1;
			});
		
			this.selectedcount =  selectedcount;
			            // pad some extra lines to fill the last page
            var padNum = this.pageSize - (cnt % this.pageSize);
            if(padNum == 8) padNum = 0;
            for(var i=0; i<padNum-1; i++) {
                $('<div class="padding"></div>').appendTo(context.$('#selections'));
            }   
            msg(cnt);
            if(cnt <= this.pageSize) {
                this.$('#buttons #prev').hide();
                this.$('#buttons #next').hide();
            } else {
                this.$('#buttons #prev').show();
                this.$('#buttons #next').show();
            }
                
        

    },
	
});    