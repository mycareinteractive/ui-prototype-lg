var dinediet = View.extend({
    
    id: 'dinediet',
    
    template: 'dinediet.html',
    
    css: 'dinediet.css',
    
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/
    navigate: function(key)    {
        var navkeys = ['LEFT','RIGHT'];
        if(navkeys.indexOf(key)!=-1) {
            this.changeFocus(key);
            return true;
        }
        
        return false;
    },
    
    click: function($jqobj) {
	var linkid = $jqobj.attr('id');
	var currLabel = this.data.label
			
	    if(linkid == 'back' ) {
            this.destroy();
            return true;
        }
                if(linkid == 'exit') {			
		    keypressed('CLOSEALL');
            keypressed(216);    //force going back to main menu

			this.destroy();
            return false;
        }

		if(linkid == 'close') {			
		    keypressed('CLOSEALL');
            keypressed(216);    //force going back to main menu

			this.destroy();
            return false;
        }

		else if(linkid == 'next') {
			// Or a speical link in the content that triggers next page
            var page = new dinemeals({viewId: 'dinemeals', breadcrumb:currLabel, data: this.subdata});
            page.render();
            return true;
        }
        
    
        
        return false;
    },
    
    renderData: function() {
		var context = this;

		
		$('#' + this.wrapper + " #dinemeals").addClass(this.classStr);
		var context = this;
		var data = this.data;

		// show the div
		$('#' + this.wrapper + " #dinediet").show();
	
		var meals = Array();
		var diets = Array();
		var dietstring = '';
		var cnt = 0;
		var MealsResult = '';
		$(data).find("GetMealsResponse").each(function() {
				MealsResult = $(data).find("GetMealsResult").html();			
		});
		var xmlObj = new DOMParser();   
		xmlObj = xmlObj.parseFromString(MealsResult, "text/xml");     
		xmlObj = MealsResult;
	var dietcount = 0;
	this.subdata = xmlObj;
	var canorderfinal = false;
	var npofinal = false;
	$(xmlObj).find("Person").each(function() { 
		$(xmlObj).find("Meals").each(function() { 
			$(xmlObj).find("Meal").each(function() { 
				var diet = $(this).attr("Diet");
				var id = $(this).attr("Id");			
				
				var canorder = $(this).attr("CanOrder");
				msg(diet.indexOf('NPO'));
                if(diet.indexOf('NPO')>=0) {
                    npofinal = true;
                }     
                  
                msg('npo ' +npofinal);
				var npo = $(this).attr("NPO");
				//if(npo == 'Y')
				//	npofinal = true;
				if(canorder == 'Y') 
					canorderfinal = true;
					
				if (typeof diets[diet] == 'undefined' && diet != '') {				
					dietstring = dietstring  + diet + '<br/>';
					diets[diet] = diet;
				}
				dietcount = dietcount + 1;
			});
		});
	});
	window.settings.totalcarbs = 0;
	var mrn = window.settings.mrn;
	var ewf = ewfObject();
	var main_url = ewf.getclinical + '?';
		//var main_url = "http://10.54.10.104:9080/ams/aceso/getClinicalData?"
		url = main_url + "type=allergies&mrn=" + mrn+ "&numrec=12&sortorder=asc";
		var dataobj = '';
		var allergies = '';
		var xml  = getXdXML(url, dataobj);		
		cnt = 0;
		 $(xml).find("item").each(function() {            		  
			if (cnt != 0) 
				allergies = allergies + ', ';
			allergies = allergies  + $(this).find("value").text();						
			cnt = cnt + 1; 
          });
		  allergies = allergies + '</p>';
		
       
		this.$('#diet').html(dietstring);
		this.$('#allergies').html(allergies);
		msg(dietstring);
		msg(dietstring + ' ' + canorderfinal + ' ' + npofinal);
        
        if(npofinal==true) {
			   	this.$('#close').attr('style','display:block');		
				this.$('#next').attr('style','display:none');		
				this.$('#back').attr('style','display:none');		
				this.$('#message').html('<br/><span>Sorry</span><br/>Your Care Team has placed you on a NPO diet which means no foods are <br/>allowed at this time. Please contact a member of your Care Team with questions');						
		} else {

		if(canorderfinal==false  && (dietstring.length>=1)) {
			   	this.$('#close').attr('style','display:block');		
				this.$('#next').attr('style','display:none');		
				this.$('#back').attr('style','display:none');		
				this.$('#message').html('<br/><span>Sorry</span><br/>We are unable to take your order at this time.  A pre-selected menu has been<br/> chosen for you.  Please contact a member of your Care Team with questions');						
		} 
        }

		if(dietstring.length==0) {
				var data = {
    	           text1: '<p>Sorry</p><br/><br><p>We are not able to take your order at this time <br/>without an active diet order  <br/>Please contact a member of your Care Team with questions</p>',
    	       };
			   
    	       var page = new Information({className:'maxselected', data: data, closeall: true});
    	       page.render();

			   return false;
		}
		
    },
    
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/

	 
});

