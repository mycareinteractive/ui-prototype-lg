var Intro2 = View.extend({
    
    id: 'intro2',
    
    template: 'intro2.html',
    
    css: 'intro2.css',
    
    className: 'agreement',
    
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/
    navigate: function(key)    {
        var navkeys = ['LEFT','RIGHT'];
        if(navkeys.indexOf(key)!=-1) {
            this.changeFocus(key);
            return true;
        }
        
        return false;
    },
    
    click: function($jqobj) {
        var linkid = $jqobj.attr('id');
        
        if(linkid == 'accept') {
            // tracking
            nova.tracker.event('language', 'set', window.settings.language);
            nova.tracker.event('terms', 'accept');
            
            // close both intro page and intro2 page
            window.pages.closePage('intro');
            this.destroy();
            // build primary page
            var page = new Primary({});
            page.render();
            return true;
        }
        else if(linkid == 'decline') {
            
            nova.tracker.event('terms', 'decline');
            
            // Reset language
            setLanguage('');
            // Close primary page
            window.pages.closePage('primary');
            // Get TV list and launch a videoplayer2 page
            this.watchTV(linkid);
            return true;
        }
        
        return false;
    },
    
    renderData: function() {
        var context = this;
    },
    
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
     watchTV: function(className) {
         var context = this;
         this.tvChannels = [];
         var channels = epgChannels().Channels;
         $.each(channels, function(i, ch) {
             context.tvChannels.push({type: 'Analog', url: ch.channelNumber, display: ch.channelName});
         });
         var page = new VideoPlayer2({viewId: 'tvonly', className:className, playlist: this.tvChannels, playlistIndex: 0, delay: 10000, delayMessage: 'One moment while we are loading the television channels...'});
         page.render();
     }
    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/

});

