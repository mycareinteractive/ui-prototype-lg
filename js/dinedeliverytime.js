var dinedeliverytime = View.extend({
    
    id: 'dinedeliverytime',
    
    template: 'dinedeliverytime.html',
    
    css: 'dinedeliverytime.css',
    
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/
    navigate: function(key)    {
        var navkeys = ['LEFT','RIGHT'];
        if(navkeys.indexOf(key)!=-1) {
            this.changeFocus(key);
            return true;
        }
        
        return false;
    },
    
    click: function($jqobj) {
                
			var currLabel = this.data.label
			var linkid = $jqobj.attr('id');

        if($jqobj.hasClass('time')) 
			this.currsel =  this.$('#selections a.active');
    
        if(linkid == 'back') {			
			$.doTimeout('dinedeliverytime clock'); // stop clock           
			this.destroy();
            return false;
        }
		if(linkid == 'order' ) {
            var currsel = this.currsel;
            if(!currsel) {
                var data = {
    	           text1: 'Warning<br/><br/><br/>Please select a delivery time',
    	       };
    	       var page = new Information({className:'dinedecline', data: data, closeall:true});
    	       page.render();				
				return true;
            }
            this._orderFood();
            return true;
        }

		this.deliveryid = linkid;
        
        
        return false;
    },
    
    renderData: function() {
		$('#' + this.wrapper + " #dinedeliverytime").addClass(this.classStr);

		var context = this;	
		// start clock
        $.doTimeout('dinedeliverytime clock', 60000, function() {
            var d = new Date();
            var format = 'dddd, mm.dd.yyyy - h:MM TT';
            var locale = window.settings.language;
            context.$('#banner #datetime').text(d.format(format, false, locale));
            return true;
        });
        $.doTimeout('dinedeliverytime clock', true); // do it now
 		
	$('#' + this.wrapper + " #dinedeliverytime").addClass(this.classStr);
	var context = this;
    var data = this.data;

	// show the div
	$('#' + this.wrapper + " #dinedeliverytime").show();
	
	var selections = this.$('#dinedeliverytime #selections');
			var deliverytimesstring = '';
            var cnt = 0;
			$(data).find("DeliveryTime").each(function() { 
				
                var time = $(this).text();            
				var id = $(this).attr("Id");						
				var lastordertime = $(this).attr("LatestOrderTime");						
				lastordertime = parseXMLTime(lastordertime);				
			
				//var lastordertime = formatdateTime("yyyy-mm-dd HH:MM:ss",lastordertime);
				//var now = formatdateTime("yyyy-mm-dd HH:mm:ss");
				var now = Date();
				datecompare = compareDates(lastordertime,now);				
                var newtime = getStandardTime(time);
                
				if(datecompare>=0) {
					deliverytimesstring = deliverytimesstring + '<a href="#" class = "time ' + time + '" time="' +newtime + '" id="' + id + '">' + newtime + '</a>';							
                    cnt = cnt + 1;   
                  }
			});
            if(cnt == 0)
                deliverytimesstring = deliverytimesstring + '<a href="#" class = "time ordernow" time="ordernow" id="ordernow">Order Now</a>';							
                
			this.$('#selections').html(deliverytimesstring);
			

    },
    
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/

    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/
		_orderFood: function () {
	        dataobj = {};
			dataobj['mrn'] = window.settings.mrn;
			dataobj['vendor'] = window.settings.vendor;
			var currsel = this.currsel;
            var deliverytime = currsel.attr("time");		
            var deliveryid = this.deliveryid;
            if(deliveryid == 'ordernow') 
                deliveryid = '';

			var mealstring = '&lt;CBORDRoomServiceOrder&gt;'					
				mealstring += '&lt;Meal Id="' + window.settings.meal 
				mealstring += '" DietOrderId="'+ window.settings.dietorder
				mealstring += '" AllergyOrderId="'
				mealstring += '" SelectedDeliveryTimeId="' + deliveryid+ '"&gt;'
				mealstring += '&lt;SelectedItems&gt;'
				
				var items = window.meals;   
				var i = 0;
               		
				for(var i=0; i<items.length; i++) {
					var item = items[i];

					var id = item["id"];
					var qty = item["qty"];
					mealstring += '&lt;Item Id="' + id + '" Quantity="' + qty + '" /&gt;'					
				}		

				mealstring += '&lt;/SelectedItems&gt;&lt;/Meal&gt;&lt;/CBORDRoomServiceOrder&gt;'			
				dataobj['orderxml'] = mealstring;
			
                var url = "http://10.54.10.104:8937/SubmitOrder"						
                var xml  = getXdXML(url, dataobj);      
                var FoodsResult = '';
                $(xml).find("SubmitOrderResponse").each(function() {
                    OrderResult = $(xml).find("SubmitOrderResult").text();			
                });
                var xmlObj = new DOMParser();   
                xmlObj = xmlObj.parseFromString(OrderResult, "text/xml");     
				var success = '', error   = '', errorT  = '';
	
                $(xmlObj).find('CBORDRoomServiceOrder').each(function(){
                status = $(this).attr('Status');
                var error = '';
                $(xmlObj).find("Message").each(function() {
                        //error = $(this).text();
                        error = $(this).attr('Id');
                });
                msg(error);
                if (status=='Ok' || status=='ok' )	{
                    if(deliverytime=='ordernow') {
                        var data = {
                            text1: 'Thank you for your order<br/><br/>Your order will be delivered within 45 minutes <br/><br/>Please call 24-3463 if you have any questions',
                        };
                    } else {
                        var data = {
                        text1: 'Thank you for your order<br/><br/>Your order will be delivered<br/>at approximately <br/>' + deliverytime + '<br/><br/>Please call 24-3463 if you have any questions',
                        };
                    }
                    var page = new Information({className:'dinedecline', data: data, closeall:true});
                    page.render();				
                    return true;
                } else {
                    var errormessage = '';
                    if(error == 'OverGoal') 
                        errormessage = 'Oops<br/><br/><br/>We are unable to process you order at this time<br/><br/>Please call 24-3463 for assistance<br/> <br/> <br/> <br/>You have exceeded your nutritional goal';        
                    else 
                        errormessage = 'Oops<br/><br/><br/>We are unable to process you order at this time<br/><br/>Please call 24-3463 for assistance<br/> <br/> <br/>';       
		
                    var data = {
                        text1: errormessage,
                    };
                    var page = new Information({className:'dinedecline', data: data, closeall:true});
                    page.render();				
                    window.meals =[];
                    return true;
                }
            });	
    return true;	
	},    	 

});

