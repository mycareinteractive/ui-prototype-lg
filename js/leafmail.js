var LeafMail = View.extend({
    
    id: 'leafmail',
    
    template: 'leafmail.html',
    
    css: 'leafmail.css',
    
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/
    navigate: function(key)    {
        if(key == 'UP' || key == 'DOWN') {
            this.changeFocus(key, '#inbox');   
            return true;
        }
        return false;   // not support keyboard
    },
    
    click: function($jqobj) {
        var linkid = $jqobj.attr('id');
        
        if(linkid == 'close') {
            this.destroy();
            return true;
        }
        
        if($jqobj.hasClass('mailitem')) {
            this._previewMail($jqobj);
            return true;
        }
        
        return false;
    },

    renderData: function() {
        var context = this;
        
        // load mails from server
        this._loadMails();
        
        // highlight the first mail
        this.focus(this.$('#inbox a:first-child'));
        this.click(this.$('#inbox a:first-child'));
    },
    
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
    
    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/
    _loadMails: function() {


	//TODO: add logic to retrieve messages from middleware
		var ewf = ewfObject();
	     // Get Patient MRN
		var mrn = this.mrn;
		if(!mrn)
			mrn = getMRNDATA();
		
		this.mrn = mrn;

		var main_url = ewf.getclinical + '?'; //"http://10.54.10.104:9080/ams/aceso/getClinicalData?"

		
		url = main_url + "type=eLeaf&mrn=" + mrn+ "&numrec=100&sortorder=asc"
		var dataobj = '';
		var allergies = '';
		var xml  = getXdXML(url, dataobj);		
        msg(xml);
		cnt = 0;
        var prevsubject = '';
        var prevdate = '';
		this.$('#inbox').empty();
		var selection = this.$('#inbox');
		 $(xml).find("item").each(function() {            		  
			var subject = $(this).find("value").text();
			var adate = $(this).find("recvdate").text();		
			var newdate = parseDateTime(adate);
			newdate = formatdateTime('mm-dd-yy',newdate);
            msg(adate + ' ' + prevdate + ' ' + subject + ' ' + prevsubject);
			if(prevdate!=adate && prevsubject != subject) {
                var $m = $('<a href="#" charset="UTF-8" class="mailitem"></a>').attr('data-index', cnt).appendTo(selection);
                $('<span class="date"></span>').text(newdate).appendTo($m);			
                $('<span class="subject ellipsis"></span>').html(subject).appendTo($m);
                cnt = cnt + 1; 
            }
            prevsubject = subject;
            prevdate = adate;
          });
		  
			  
    },
    
    _previewMail: function($obj) {
        // expand the message to the body
        var content = $obj.find('span.subject').html();
		this.$('#content #body p').html(content);
    }
});


