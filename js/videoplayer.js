var VideoPlayer = View.extend({

    id: 'videoplayer',
    
    template: 'videoplayer.html',
    
    css: 'videoplayer.css',
    
    type: 'education video',
    
    bookmark: true,
    restart: false,
    allowTrickMode: true,
    
    asset: null,
    ticket: null,
    
    state: '',
    scale: '1.0',
    seconds: 0.0,
    noPosition: true,
    
    windowMode: true,
    windowModeX: 46,
    windowModeY: 0,
    windowModeWidth: 1188,
    windowModeHeight: 668,
    fullscreenX: 0,
    fullscreenY: 0,
    fullscreenWidth: 1280,
    fullscreenHeight: 720,
    
    html5VideoType: '',
    
    rtspUrl: 'rtsp://%serverIp%:%serverPort%/%appId%?assetUid=%assetUid%&transport=%transport%&ServiceGroup=%serviceGroup%&smartcard-id=%deviceId%&device-id=%deviceId%&home-id=%homeId%&client_mac=%deviceId%&purchase-id=%ticketId%',
    
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/

    navigate: function(key)	{
    	var handled = true;
        
        switch(key) {
            case 'PLAY':
            case 'PAUS':
                if(this.state == 'play' && this.scale*1 == 1.0) {
                    this._trackEvent('pause');
                    this.pauseVideo();
                }
                else {
                    this.normalVideo();
                    this._trackEvent('resume');
                }
                break;
            case 'RWND':
                this.rewindVideo();
                this._trackEvent('rewind');
                break;
            case 'FFWD':
                this.forwardVideo();
                this._trackEvent('forward');
                break;
            case 'VIDEO': // toggle between fullscreen and windowed TV mode
                this.toggleWindowMode();
                break;
            case 'STOP':
                this.destroy();
                this._trackEvent('stop');
                break;
            case 'LEFT':
            case 'RIGHT':
                this.changeFocus(key, '#controlbar #buttons');
                break;
            default:
                handled = false;
        }
        
        return handled;
    },
    
    // When "ENTER" is pressed on a link, or when link is clicked by mouse or touch screen
    click: function($jqobj) {
    	var linkid = $jqobj.attr('id');
        var parentid = $jqobj.parent().attr('id');
        	
    	if(linkid == 'stop') { // stop button
    		this.destroy();
    		return true;
    	}
    	else if(linkid == 'fullscreen') {   // full screen button
            this.setWindowMode(false);
            return true;
        }
        else if(linkid == 'exitfullscreen') { // exit full screen
            this.setWindowMode(true);
            return true;
        }

    },
    
    focus: function($jqobj) {
        this._super($jqobj);
    },
    
    blur: function($jqobj) {
    	this._super($jqobj);
    },
    
    renderData: function() {
        msg('videoplayer renderData');
        var context = this;
        
        var title = '';
        var duration = '00:00:00';
			
        this._getAsset();   // get asset info
        
        if(this.bookmark) { // get ticket info if requires bookmarking
            this._getTicket();
        }
        
        if(!this.asset) {
            // something is wrong, give up
            this.videoError(1, '', 'Unable to retrieve video information');
            return;
        }
        
        var asset = this.asset;
        
        if(asset.ListOfMetaData) {
            title = asset.ListOfMetaData.Title;
            this.$('#progress p.title').text(title);
            this.label = 'Playing ' + title;
        }
        if(asset.tagAttribute) {
            duration = this._calcDuration(asset.tagAttribute.duration);
            this.$('#progress p.duration').text(duration);
        }
        
        this._updateCurrent();
    },
    
    uninit: function() {
        // stop player
        this.stopVideo();
        
        // stop clock
        $.doTimeout('video npt'); 
    },
    
    shown: function() {
        
        var context = this;
        
        this.startVideo();
        
        // analytics tracking
        this._trackEvent('play');
        
        $.doTimeout('video npt', 1000, function() {
            if(context.state == 'play') {
                context.seconds = context.seconds + context.scale*1;
                if(context.seconds < 0.0)
                    context.seconds = 0.0;
            }
            context._updateCurrent();
            return true;
        });
        
        var $firstObj = this.$('#buttons a:first');
        this.focus($firstObj);
    },
    
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
    startVideo: function() {
        msg('videoplayer startVideo');
        var ewf         = ewfObject();
        var serverload  = getServerLoadDATA();
        
        var serverIp = serverload.IPAddress || '192.168.100.80';
        var serverPort = serverload.Port || '554';
        var appId = this.bookmark? '60010001': '60010000';
        var transport = ewf.RTSPTransport_Enseo;
        var serviceGroup = ewf.ServiceGroup;
        var deviceId = window.settings.mac;
        var homeId = window.settings.homeID;
        var assetUid = dec2hex(this.asset.tagAttribute.entryUID);
        
        var ticketId = '';
        if(this.ticket && this.ticket.tagAttribute && this.ticket.tagAttribute.ticketID)
            ticketId = this.ticket.tagAttribute.ticketID;
        
        var npt = ['0.00',''];
        if(this.bookmark && !this.restart) {    // we want to resume from suspend position
            if(this.ticket && this.ticket.tagAttribute && this.ticket.tagAttribute.suspendPosition) {
                npt[0] = this.ticket.tagAttribute.suspendPosition;
                this.seconds = npt[0]*1;
            }
        }
        
        var videoUrl = this.asset.ListOfMetaData.ContentUrl;
        var poster = this.asset.ListOfMetaData.PosterBoard;
        
        if(!videoUrl)
        	this.html5VideoType = null;
        else if(videoUrl.indexOf('youtube.com')>0)
            this.html5VideoType = 'youtube';
        else if(videoUrl.indexOf('vimeo.com')>0)
            this.html5VideoType = 'vimeo';
        else
            this.html5VideoType = videoUrl.substr(videoUrl.lastIndexOf('.')+1);
            
        var version = window.settings.version;
        
        
        if(this.html5VideoType) {
            var player = Nebula.getHtml5Player();
            player.play(this.$('#videopanel'), [{src:videoUrl, type:'video/'+this.html5VideoType}], poster);
            this.setWindowMode(this.windowMode);
        }
        else {
            
            if(version == 'NEBULA') {
                var url = this.rtspUrl;
                
                url = url.replace('%serverIp%', serverIp).replace('%serverPort%', serverPort)
                    .replace('%appId%', appId).replace('%assetUid%', assetUid)
                    .replace('%transport%', transport).replace('%serviceGroup%', serviceGroup)
                    .replace(/%deviceId%/g, deviceId).replace('%homeId%', homeId)
                    .replace('%ticketId%', ticketId);
                    
                var player = Nebula.getRtspClient();
                player.setup(url, this.videoError, this.videoResponse, this.videoAnnounce, this);
                player.play(1.0, npt);
                player.setTVLayerEnable(true);
                this.setWindowMode(this.windowMode);
            }
        }
        
    },
    
    stopVideo: function() {
        msg('videoplayer stopVideo');
        
        if(this.bookmark && !this.html5VideoType) { // update suspend position
            var position = '0.000';
            if(!this.noPosition) {
                position = Math.floor(this.seconds) + '.000';
            }
            
            var ticketId = '';
            if(this.ticket && this.ticket.tagAttribute && this.ticket.tagAttribute.ticketID)
                ticketId = this.ticket.tagAttribute.ticketID;
            
            getUpdateSuspendPosition(ticketId, position);
        }
        
        var version = window.settings.version;
        if(this.html5VideoType) {
            var player = Nebula.getHtml5Player();
            player.stop();
        }
        else {
            if(version == 'NEBULA') {
                var player = Nebula.getRtspClient();
                player.setTVLayerEnable(false);
                player.stop();
            }
        }
    },
    
    pauseVideo: function() {
        var version = window.settings.version;
        if(version == 'NEBULA') {
            var player = Nebula.getRtspClient();
            player.pause();
            player.setTVLayerEnable(true);
            this.setWindowMode(this.windowMode);
        }
    },
    
    normalVideo: function(restart) {
        var version = window.settings.version;
        if(version == 'NEBULA') {
            var range = restart? ['0.0','']: '';
            if(this.seconds < 5.0) {    // beginning
                range = ['0.0',''];
            }
            var player = Nebula.getRtspClient();
            player.play(1.0, range);
            player.setTVLayerEnable(true);
            this.setWindowMode(this.windowMode);
        }
    },
    
    forwardVideo: function() {
        var version = window.settings.version;
        if(version == 'NEBULA') {
            var player = Nebula.getRtspClient();
            player.play(7.5);
            player.setTVLayerEnable(true);
            this.setWindowMode(this.windowMode);
        }
    },
    
    rewindVideo: function() {
        var version = window.settings.version;
        if(version == 'NEBULA') {
            var player = Nebula.getRtspClient();
            player.play(-7.5);
            player.setTVLayerEnable(true);
            this.setWindowMode(this.windowMode);
        }
    },
    
    videoError: function(error, code, message) {
        msg('videoplayer error ' + error + ' (' + code + ' ' + message + ')');
        switch(error) {
            case 1:
            case 9:
            case 20:
            case 21:
            case 22:
            case 23:
                this.stopVideo();
                this.$('#popup').text('Error ' + error + ' (' + code + ' ' + message + ')').show();
                break;
        }
    },
    
    videoResponse: function(resp) {
        var respstr = JSON.stringify(resp);
        msg('videoplayer response ' + respstr);
        resp = JSON.parse(respstr);
        
        var scale, range, npt;
        if(resp['Code'] != '200') {
            msg('RTSP request failed with error code ' + resp['Code']);
            
            if(resp['Method']=='SETUP') {
                // track setup failure
                this._trackEvent('fail');
            }
            return;
        }
        
        switch(resp['Method']) {
            case 'SETUP':
                break;
                
            case 'PLAY':
                scale = resp['Scale:'];
                range = resp['Range:'];
                if(range)
                    npt = parseFloat(range.split('=')[1]);
                
                if(scale)
                    this.scale = scale * 1;
                if(npt)
                    this.seconds = npt * 1;
                
                this.state = 'play';    
                break;
                
            case 'PAUSE':
                this.state = 'pause';
                break;
                
            default:
                break;
        }
    },
    
    videoAnnounce: function(annc) {
        annc = JSON.parse(JSON.stringify(annc));
        var notice = annc['TianShan-Notice:'];
        msg('videoplayer announce ' + ' (' + notice + ')');
        if(!notice)
            return;
        
        if(notice.indexOf('State Changed')>=0) {
            // state changed
            this.noPosition = false;
            
            var state = annc['presentation_state'];
            if(state)
                this.state = state;
            
            var npt = annc['npt'];
            if(npt) {
                this.seconds = npt*1;
            }
        }
        else if(notice.indexOf('End-of-Stream')>=0) {
            // reaches end, stop video
            this.noPosition = true;
            this.scale = 0.0;
            this.destroy();
        }
        else if(notice.indexOf('Beginning-of-Stream')>=0) {
            // reaches beginning, play again
            this.normalVideo(true);
        }
        else if(notice.indexOf('Scale Changed')>=0) {
            var scale = annc['Scale'];
            if(scale)
                this.scale = scale;
                
            var npt = annc['npt'];
            if(npt) {
                this.seconds = npt*1;
            }
        }
    },
    
    toggleWindowMode: function() {
        msg('toggleWindowMode');
        if(this.windowMode) { 
            // go fullscreen
            this.setWindowMode(false);
        }
        else {
            // shrink to window mode, and update EPG info
            this.setWindowMode(true);
        }
    },
    
    setWindowMode: function(windowMode) {
        var platform = window.settings.version;
        
        this.windowMode = windowMode;
        if(!this.windowMode) { 
            // fullscreen
            this.$('#videopanel').removeClass('windowmode').addClass('fullscreen');
            this.$('#exitfullscreen').show();
            if(this.html5VideoType) {
                var player = Nebula.getHtml5Player();
                if (player) {
                    player.setVideoLayerRect(this.fullscreenX, this.fullscreenY, this.fullscreenWidth, this.fullscreenHeight);
                }
            }
            else {
                if(platform == 'NEBULA') {
                    var player = Nebula.getRtspClient();
                    if (player)
                        player.setTVLayerRect(this.fullscreenX, this.fullscreenY, this.fullscreenWidth, this.fullscreenHeight);
                }
                else if(platform == 'ENSEO') {
                    var player = Nimbus.getPlayer();
                    if (player)
                        player.setVideoLayerRect(this.fullscreenX, this.fullscreenY, this.fullscreenWidth, this.fullscreenHeight);
                }
            }
        }
        else {
            // window mode
            this.$('#videopanel').removeClass('fullscreen').addClass('windowmode');
            this.$('#exitfullscreen').hide();
            if(this.html5VideoType) {
                var player = Nebula.getHtml5Player();
                if (player) {
                    player.setVideoLayerRect(this.windowModeX, this.windowModeY, this.windowModeWidth, this.windowModeHeight);
                }
            }
            else {
                if(platform == 'NEBULA') {
                    var player = Nebula.getRtspClient();
                    if (player)
                        player.setTVLayerRect(this.windowModeX, this.windowModeY, this.windowModeWidth, this.windowModeHeight);
                }
                else if(platform == 'ENSEO') {
                    var player = Nimbus.getPlayer();
                    if (player)
                        player.setVideoLayerRect(this.windowModeX, this.windowModeY, this.windowModeWidth, this.windowModeHeight);
                }
            }
        }
    },
    
    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/
    _getAsset: function () {
	
		
        var asset;
        if (this.data.tagName == 'Ticket' && this.data.ListOfSubEntry) {  // a ticket
            $.each(this.data.ListOfSubEntry, function(j, sub){
                if(sub.tagName == 'Asset') {
                    asset = sub;
                    return false;
                }
            });
        }
        else if (this.data.tagName == 'Asset') { // an asset
            asset = this.data;
        }
        else if (typeof this.data == 'string') { // asset ID
            var assetData = getGetAsset(this.data);
            if(!assetData || !assetData.DataArea || assetData.DataArea.tagName!='Asset') {
                msg('get asset ' + this.data + ' failed');
                return false;
            }
            asset = assetData.DataArea;
            this.bookmark = false;
        }
        
        this.asset = asset;
        return true;
    },
    
    _getTicket: function(ticketId) {
        if (this.data.tagName == 'Asset') {
            var ticketId = this.asset.tagAttribute.ticketIDList;
            if(!ticketId || ticketId=='' || ticketId=='null')
                this._getTicketByPurchase();
            else
                this._getTicketByTicketId();
        }
        else if(this.data.tagName == 'Ticket') {
            this.ticket = this.data;
        }
    },
    
    _getTicketByTicketId: function() {
        var ticket = getGetTicket(this.asset.tagAttribute.ticketIDList);
        if(!ticket || !ticket.DataArea || ticket.DataArea.tagName!='Ticket') {
            msg('get ticket for asset ' + this.asset.tagAttribute.productOfferingUID + ' ticket ' + this.asset.tagAttribute.ticketIDList + ' failed');
            return false;
        }
        this.ticket = ticket.DataArea;
        return true;
    },
    
    _getTicketByPurchase: function() {
        var transaction = getPurchaseProduct(this.asset.tagAttribute.productOfferingUID, this.asset.tagAttribute.localEntryUID);
        if(!transaction || !transaction.DataArea || transaction.DataArea.tagName!='Transaction') {
            msg('purchase asset ' + this.asset.tagAttribute.productOfferingUID + ' failed');
            return false;
        }
        var t = transaction.DataArea;
        if(t && t.ListOfTicket && t.ListOfTicket[0]) {
            this.ticket = t.ListOfTicket[0];
        }
        return true;
    },
    
    _updateCurrent: function() {
        var current = this._calcDuration(this.seconds);
        this.$('#progress p.current').text(current);
    },
    
    _calcDuration: function (duration) {
        if(!duration)
            return '0:00:00';
        
        duration = Math.floor(duration);
        
        var str = '';
        var h = Math.floor(duration/3600);
        str += h + ':';
        duration = duration%3600;
        
        var mm = Math.floor(duration/60);
        str += (mm<10?'0':'') + mm + ':';
        duration = duration%60;
        
        var ss = duration;
        str += (ss<10?'0':'') + ss;
        return str;
    },
    
    _trackEvent: function(action) {
        var label = this.asset.tagAttribute.localEntryUID;
        if(this.asset.ListOfMetaData) {
            label +=  '|' + this.asset.ListOfMetaData.Title;
        }
        nova.tracker.event(this.type, action, label);
    }
});    