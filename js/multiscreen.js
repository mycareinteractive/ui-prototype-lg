var Multiscreen = View.extend({
    
    id: 'multiscreen',
    
    template: 'multiscreen.html',
    
    css: 'multiscreen.css',
    
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/
    navigate: function(key)    {
        return false;   // not support keyboard
    },
    
    click: function($jqobj) {
        var linkid = $jqobj.attr('id');
        var parentid = $jqobj.parent().attr('id');
        if($jqobj.hasClass('close-button') || $jqobj.hasClass('back')) {
        	if(this.closeall==true) {			
				keypressed('CLOSEALL');
				keypressed(216);    //force going back to main menu
			}
				
    		this.destroy();
    		return true;
        }
        return false;
    },

    renderData: function() {
        var context = this;
        
        this.$('#content #header #name').text(this.data.name);
        if(window.pubnub && this.data.subchannel) {
        	try {
	        	msg('subscribing to channel ' + this.data.subchannel);
	        	window.pubnub.subscribe({
			        channel : this.data.subchannel,
			        message : function(m){
			        	msg('multiscreen message received ' + JSON.stringify(m));
			        	if(typeof m == 'string') {
			        		context.$('#content #body #payload').html(m);
			        	}
			        	else if(m.action == 'stop') {
			        		context.destroy();
			        	}
					}
			    });
	        	window.pubnub.publish({
	        		channel : this.data.subchannel,
	        		message : {action: 'join', uuid: window.settings.version + '-' + window.settings.platformVersion + '-' + window.settings.mac}
	        	});
        	}
        	catch(e){}
        }
    },
    
    uninit: function() {
    	if(window.pubnub) {
    		try {
	    		window.pubnub.publish({
	        		channel : this.data.subchannel,
	        		message : {action: 'leave', uuid: window.settings.version + '-' + window.settings.platformVersion + '-' + window.settings.mac}
	        	});
	    		
	        	window.pubnub.unsubscribe({
	        		channel: this.data.subchannel
	        	});
    		}
    		catch(e){}
        }
    }
    
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
    
    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/
});



