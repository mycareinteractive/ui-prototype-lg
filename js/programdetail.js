var ProgramDetail = View.extend({

    id: 'programdetail',
    
    template: 'programdetail.html',
    
    css: 'programdetail.css',
    
    pageSize: 6,
    
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/

    navigate: function(key)	{
    	if(key == 'UP' || key == 'DOWN') {
    		this.changeFocus(key, '#buttons');	
    		return true;
    	}
    	return false;
    },
    
    // When "ENTER" is pressed on a link, or when link is clicked by mouse or touch screen
    click: function($jqobj) {
    	var linkid = $jqobj.attr('id');
        var parentid = $jqobj.parent().attr('id');
        var type = $jqobj.attr('data-type');
        	
    	if($jqobj.hasClass('back')) { // back button
    		this.destroy();
    		return true;
    	}
    	
    	if(linkid == 'remove') {   // remove bookmark
    	    var ticketId = '';
    	    if (this.data.tagName == 'Ticket' && this.data.tagAttribute) {
    	        ticketId = this.data.tagAttribute.ticketID;
            }
            else if (this.data.tagName == 'Asset' && this.data.tagAttribute) {
                ticketId = this.data.tagAttribute.ticketIDList;
            }
            
            if(ticketId && ticketId != '') {
                cancelAsset(ticketId);
            }
            
            if(this.parent && this.parent.refresh)
                this.parent.refresh();
                
            this.destroy();
            return true;
    	}
    	else if(linkid == 'add') {  // add bookmark
    	    var entryId = '', poId = '';
    	    if (this.data.tagName == 'Asset' && this.data.tagAttribute) {
                entryId = this.data.tagAttribute.localEntryUID;
                poId = this.data.tagAttribute.productOfferingUID;
            }
            
            if(entryId && ticketId != '' && poId && poId != '') {
                purchaseAsset(poId,entryId);
            }
    	    
    	    if(this.parent && this.parent.refresh)
                this.parent.refresh();
                
    	    this.destroy();
            return true;
    	}
    	else if(linkid == 'play') { // play video
    	    var breadcrumb = this.$('#label p').text();
    	    var page = new VideoPlayer({className:'', parent:this, breadcrumb:breadcrumb, data: this.data, bookmark: true});
            page.render();
            return true;
    	} 

    },
    
    focus: function($jqobj) {
        if($jqobj.parent().attr('id') == 'buttons') {
            this._super($jqobj);
    	}
    },
    
    blur: function($jqobj) {
    	this._super($jqobj);
    },
    
    renderData: function() {
        var context = this;
        var isBookmarked = false;
        
        // fill in title and duration
        $('<p></p>').html(this.breadcrumb).appendTo(this.$('#breadcrumb'));
        if (this.data.tagName == 'Ticket' && this.data.ListOfSubEntry) {  // a ticket from MyPrograms
            isBookmarked = true;
            $.each(this.data.ListOfSubEntry, function(j, sub){
                if(sub.tagName == 'Asset' && sub.ListOfMetaData) {
                    var title = sub.ListOfMetaData.Title;
                    var duration = context._calcDuration(sub.tagAttribute.duration);
                    var description = sub.ListOfMetaData.LongDescription;
                    //$('<p class="title ellipsis"></p>').text(title).appendTo(context.$('#selections'));
                    //$('<p class="duration"></p>').text(duration).appendTo(context.$('#selections'));
                    context.$('#selection #description').text(description);
                    context.$('#selections p.duration span').text(duration);
                    
                    context.label = title;
                    
                    return false;
                }
            });
        }
        else if (this.data.tagName == 'Asset') { // an asset from AllPrograms
            var sub = this.data;
            if(sub.tagAttribute && sub.tagAttribute.ticketIDList && sub.tagAttribute.ticketIDList != '') {
                isBookmarked = true;
            }
            if(sub.ListOfMetaData) {
                var title = sub.ListOfMetaData.Title;
                var duration = context._calcDuration(sub.tagAttribute.duration);
                var description = sub.ListOfMetaData.LongDescription;
                //$('<p class="title ellipsis"></p>').text(title).appendTo(context.$('#selections'));
                //$('<p class="duration"></p>').text(duration).appendTo(context.$('#selections'));
                context.$('#selection #description').text(description);
                context.$('#selections p.duration span').text(duration);
                
                context.label = title;
            }
        }
        
        // determine which buttons to show up
        if(isBookmarked) {
            this.$('#buttons a#add').hide();
            this.$('#buttons a#remove').show();
            this.$('#selection p#toadd').hide();
            this.$('#selection p#toremove').show();
        }
        else {
            this.$('#buttons a#add').show();
            this.$('#buttons a#remove').hide();
            this.$('#selection p#toadd').show();
            this.$('#selection p#toremove').hide();
        }
    },
    
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
    
    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/
    _calcDuration: function (duration) {
        if(!duration)
            return '00:00';
        var str = '';
        if(duration>=3600) {
            var h = Math.floor(duration/3600);
            str += h + ':';
            duration = duration%3600;
        }
        if(duration>=60) {
            var mm = Math.floor(duration/60);
            str += (mm<10?'0':'') + mm + ':';
            duration = duration%60;
        }
        var ss = duration;
        str += (ss<10?'0':'') + ss;
        return str;
    }
});    