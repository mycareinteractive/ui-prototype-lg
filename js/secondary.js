var Secondary = View.extend({

    id: 'secondary',
    
    template: 'secondary.html',
    
    css: 'secondary.css',
    
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/

    navigate: function(key) {
        if(key == 'UP' || key == 'DOWN') {
            this.changeFocus(key, '#selections');   
            return true;
        }
        if(key == 'LEFT' || key == 'RIGHT') {
            this.changeFocus(key, '#selection #body', '.submenu');
            return true;
        }
        return false;
    },
    
    // When "ENTER" is pressed on a link, or when link is clicked by mouse or touch screen
    click: function($jqobj) {
        var currLabel = this.data.label;
        var linkid = $jqobj.attr('id');

        var type = $jqobj.attr('data-type');
        var datalink = $jqobj.attr('data-link');
        var dataenter = $jqobj.attr('data-enter');
        var videolink = $jqobj.attr('data-video');
        var text = $jqobj.text();
        msg('video link' + videolink);

        var itemData;       
        var menuId = this.$('#selections').find('.active').attr('id');
        var path;
        if(videolink) { // video needs deep link track
            path = this.pagePath + '/' + menuId + '/' + linkid;
            if(videolink.indexOf('http') >= 0) {
            	var page = new VideoPlayer2({className:linkid, parent:this, breadcrumb:currLabel, bookmark: false, pagePath: path, type: 'HTML5', url: videolink, display: text});
            	page.render();
            }
            else {
	            var page = new VideoPlayer({className:linkid, parent:this, breadcrumb:currLabel, data: videolink, bookmark: false, pagePath: path});
	            page.render();
            }
            return true;
        }
        if(dataenter) {
            $activemenu = this.$('#selections #'+dataenter);
            
            itemData = this._getMenuItemData($activemenu);
            
            linkid = dataenter;
            msg(itemData);
        } else {
            itemData = this._getMenuItemData($jqobj);
        }
            
        if($jqobj.hasClass('back')) { // back button
            this.destroy();
            return true;
        } 
        
        // menu item is a menu
        // open another secondary page, give it a different viewId
        
        // Uncomment the following to suppor the keyboard navigation
        /*
        if(itemData && itemData['type']=='menu'){           
            var page = new Secondary({viewId: 'secondary2', css: null, className:linkid, breadcrumb:currLabel, data: itemData});
            page.render();
            return true;
        }
        */
        // Or a speical link in the content that triggers next page
            
        msg(dataenter );
        if(dataenter=='mymenu') {
            var primary = window.pages.findPage('primary');
            if(primary && primary.open) {
                primary.open.call(primary, dataenter);
            }
        }
        
        if(dataenter && itemData.type != 'link') {
            var page = new Secondary({viewId: 'secondary2', css: null, className:linkid, breadcrumb:currLabel, data: itemData});
            page.render();
            return true;
        }
        
        
        // menu item is a link
        if(itemData && itemData.type == 'link') {
            var page;
            if($jqobj.parent().attr('id')=='selections')  {
                page = new WebSite({className:linkid, breadcrumb:currLabel, data: itemData});
            }
            else { // link inside content needs to deep link path
                path = this.pagePath + '/' + menuId + '/' + linkid;
                page = new WebSite({className:linkid, breadcrumb:currLabel, data: itemData, pagePath: path});
            }
            page.render();
            return true;
        }
        
        // menu item is a shortcut
        if(itemData && itemData.type == 'shortcut') {
            this.destroy();
            if(itemData.attributes && itemData.attributes.attribute) {
                var attr = itemData.attributes.attribute[0] || itemData.attributes.attribute;
                var tagName = attr.text;
                var primary = window.pages.findPage('primary');
                if(primary && primary.open) {
                    primary.open.call(primary, tagName);
                }
            }
            return true;
        }
        
        // track link clicks as a pageview
        if($jqobj.parent().attr('id')=='selections') { // is link on the left
            var path = this.pagePath + '/' + $jqobj.attr('id');
            var title = $jqobj.text();
            nova.tracker.pageView(path, title);
        }
        
        
        return false;
    },
    
    focus: function($jqobj) {
        this._super($jqobj);
        if(!$jqobj.hasClass('back')) {
            $('#secondary #background').addClass('textoverlay');
            this._renderSubData($jqobj);
        }
    },
    
    blur: function($jqobj) {
        if($jqobj.parent().attr('id')=='selections') {
            this._super($jqobj);
        }
        
        this.$('#selection #body').html('');
    },
    
    renderData: function() {

        var data = this.data;
        $('<p></p>').html(this.breadcrumb).appendTo(this.$('#breadcrumb'));
        
        for(var key in data) {
            if (key=='tag') {   // tag
                this.query = 'tag=' + escape(data[key]);
            }
            else if(key=='label') { // label
                var c = $('<p></p>').html(data[key]);
                this.$('#label').append(c);
                this.label = data[key];
            }
            
            else if(key=='attributes') { // attributes (title and label icon

                var attrlist = data.attributes.attribute;
                var len = attrlist.length;
                for(var i=0; i<len; i++) {
                    var attr = attrlist[i];
                    if (attr.class == 'banner1') {
                        this.$('#banner').html('<p class="banner">' + attr.text + '</p>');              
                    } 
                    else if (attr.class == 'instructions') {
                        this.$('#instructions').html('<p class="i">' + attr.text + '</p>');             
                    } 
                    else if (attr.class == 'banner2') {
                        var c = $('<span></span>').html('<br/>' + attr.text);
                        this.$('#banner p').append(c);              
                    }
                    else if (attr.class == 'icon') {
                        this.$('img#labelicon').attr('src', './images/'+attr.image);
                    }           
                }
            }
            else if(key==data.tag) { // sub menu or children
                
                this.subdata = data[key];
                var sub = this.data[key];           
                var context = this;
                var style = '';
                if(typeof data[key].label !='undefined') {                  
                    if(data[key].label == 'nobutton') {
                        style ='display:none';
                    } else {
                        style ='display:block';
                    }
                    var o = $('<a href="#"></a>').html(data[key].label).attr('id',data[key].tag).attr('title',data[key].tag).attr('style', style).attr('data-type', data[key].type).addClass('menu');                                                   
                    this.$('#selections').append(o);
                } 
                else {
                    $.each(data[key], function(i, v) {
                        if(v.label == 'nobutton') {
                            style ='display:none';
                        } else {
                            style ='display:block';
                        }

                            var o = $('<a href="#"></a>').html(v.label).attr('id',v.tag).attr('title',v.tag).attr('style', style).attr('data-type',v.type).addClass('menu');
                            context.$('#selections').append(o);   
                    });
                }
                
                this.$('#selections').append('<a class="back" href="#" title="back">Back</a>');
            }
        }        
    },
    
    shown: function() {
        var $firstObj = this.$('#selections a:first');
        $firstObj.click();
    },
    
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
    
    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/
    
    _renderSubData: function() {
        var dataidx = this.getIndex('#selections');
        if(dataidx>=0 && this.subdata[dataidx]) {
           var sub = this.subdata[dataidx];
        } else {
           var sub = this.subdata;
        }
        
        if(!sub)
           return;
           
        var $left = this.$('#selections').find('.active');
        this.$('#selection #body').attr('data-parent', $left.attr('id'));
           
        if(sub['type']=='page' || sub['type']=='menu') {
            this._renderPage(sub);
        }
        else if(sub['type']=='pagelist') {
            this._renderPageList(sub);
        }
    },
    
    _renderPage: function(subdata) {
        var sub = subdata;
        if(sub && sub['attributes'] && sub.attributes.attribute) {
            var attrlist = sub.attributes.attribute;
            var len = attrlist.length;
            
            if(typeof attrlist.class !='undefined') {
                if(attrlist.class=='body') { // text in page
                    this.$('#selection #body').html('<p class="submenu active">' + attrlist.text + '</p>');
                } else if(attrlist.class=='footer') { // text in page                   
                    var o = $('<p class="submenu active topmargin"></span>').html(attrlist.text);
                    this.$('#selection #body').append(o);                                   
                } else if (attrlist.class == 'banner1') {
                    this.$('div#banner').html('<p class="banner">' + attrlist.text + '</p>');               
                } else if (attrlist.class == 'banner2') {
                    var o = $('<span></span>').html('<br/>' + attrlist.text);
                    this.$('div#banner p').append(o);                   
                } else if (attrlist.class == 'instructions') {
                    this.$('div#instructions').html('<p class="instructions">' + attrlist.text + '</p>');               
                }
            
        
            } else
            for(var i=0; i<len; i++) {
            
                var attr = attrlist[i];
                
                if(attr.class=='body') { // text in page
                    this.$('#selection #body').html('<p class="submenu active">' + attr.text + '</p>');
                } else if(attr.class=='footer') { // text in page                   
                    var o = $('<p class="submenu active highlight topmargin"></span>').html(attr.text);
                    this.$('#selection #body').append(o);                                   
                }   if (attr.class == 'banner1') {
                    this.$('div#banner').html('<p class="banner">' + attr.text + '</p>');               
                } else if (attr.class == 'banner2') {
                    var o = $('<span></span>').html('<br/>' + attr.text);
                    this.$('div#banner p');                 
                } else if (attrlist.class == 'instructions') {
                    this.$('div#instructions').html('<p class="instructions">' + attrlist.text + '</p>');               
                }
            }
        }
    },
    
    _renderPageList: function(subdata) {
        var sub = subdata;
        if(sub && sub['attributes'] && sub.attributes.attribute) {
            var attrlist = sub.attributes.attribute;
            var len = attrlist.length;
            var isPage1 = true;
            var pagebody = this.$('#selection #body').html('');
            var pages = $('<div id="pages"></div>');
            pagebody.append(pages);
            var page = 0;
            for(var i=0; i<len; i++) {
                var attr = attrlist[i];
                if(attr.class=='body') { // text in pagelist
                if(isPage1) {
                    isPage1 = false;
                    attrstr = ' class="submenu active"';
                }
                else {
                    attrstr=' class="submenu"'; 
                }
                page ++;            
                msg('sub.tag ' + sub.tag);
                if(this.data.tag=='directory') {
                    this.$('div#banner').html('<p class="banner">Use Right / Left to turn the pages of the Hospital Directory.</p>');               
                    var o = $('<span></span>').html('<br/>');
                    this.$('div#banner p').append(o);                                   
                    this.$('div#instructions').html('<p class="instructions">Spaulding has been designed to offer a patient-focused environment of healing. We invite you to explore our hospital. </p>');              
                    pages.append('<p id=p' + i + attrstr + '>' + '<span class="rjust" >&lt; Page ' + page + ' &gt; </span><br/> '+attr.text +'<span class=bottom>Floor 1 includes Outdoor Plaza</span>' +'</p>');
                    } else
                if(sub.tag=='medicalstaff') {
                    this.$('div#banner').html('<p class="banner"> Use &lt; Left / Right &gt; Arrow keys to scroll pages </p>');             
                    var o = $('<span></span>').html('<br/>');
                    this.$('div#banner p').append(o);                                   
                    pages.append('<p id=p' + i + attrstr + '>' + '<span class="rjust" >&lt; Page ' + page + ' &gt; </span><br/> '+attr.text +'</p>');
                    }
                }
                
            }
            
            // support mouse driven page up/down
            var context = this;
            $('<div id="nextpage"></div>').appendTo(pagebody).click(function(e){
                context.changeFocus('RIGHT', '#selection #body', '.submenu');
                e.preventDefault();
                return true;
            });
            $('<div id="prevpage"></div>').appendTo(pagebody).click(function(e){
                context.changeFocus('LEFT', '#selection #body', '.submenu');
                e.preventDefault();
                return true;
            });
        }
        return;
    },
    
    _getMenuItemData: function ($obj) {
        var itemData = null;
        var itemTag = $obj.attr('id');
        var parentTag = $obj.parent().attr('id');
        
        var items = this.data[this.data.tag];
        if($.isArray(items)){ // an array of items
            
            $.each(items, function(i,item){
                if(item.tag == itemTag) {
                    itemData = item;
                    return false;   // break
                }
            });
            
        }
        else { // only one item
            if(items.tag == itemTag)
                itemData = items;
        }
        
        return itemData;
    },
    /*
    _openDine: function(linkid, breadcrumb, data) {
        var data    = getUserDataDATA();
        var context = this;
        var page = new dineintro({className:linkid, breadcrumb:breadcrumb, data: data,
            oncreate: function() {
                context.updateNavbar();
            }, 
            ondestroy: function() {
                context.updateNavbar();
            }
        });
        page.render();
    }
    */
    
});    