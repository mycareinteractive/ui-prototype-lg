var Languages = View.extend({
    
    id: 'languages',
    
    template: 'languages.html',
    
    css: 'languages.css',
    
    language: null,
    
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/
    navigate: function(key)    {
        return false;   // not support keyboard
    },
    
    click: function($jqobj) {
        var linkid = $jqobj.attr('id');
        var parentid = $jqobj.parent().attr('id');
        
        if(linkid == 'cancel') {
            this.destroy();
            return true;
        }
        
        if(linkid == 'translate') {          
            var lang = this.language || 'en';
                       
            //save it
            window.settings.language = lang;
            // send language back to server
            setLanguage(lang); 
            
            // tracking
            nova.tracker.event('language', 'switch', lang);
            
            // just close, the primary page will detect language change and translate itself
            this.destroy();
            return true;
        }
        
        if(parentid == 'selections') {  // choose one language
            this.language = linkid;
            this.$('#body div').hide();
            this.$('#body #disclaimer-' + linkid).show();
            if(linkid == window.settings.language) {
                this.$('#buttons #translate').hide();
            }
            else {
                this.$('#buttons #translate').show();
            }
            this.translate(linkid); // translate current page
            return true;
        }
        
        return false;
    },

    renderData: function() {
        var context = this;
        
        this.$('#body div').hide();
        this.$('#body #instruction').show();
        this.$('#buttons #translate').hide();
    }
    
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
    
    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/
});



