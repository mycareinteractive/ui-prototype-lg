
var dinesandwich = View.extend({

    id: 'dinesandwich',
    
    template: 'dinesandwich.html',
   
    css: 'dinesandwich.css',
    
    pageSize: 8,

	course: null,
    
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/

    navigate: function(key)	{
    	if(key == 'UP' || key == 'DOWN') {
    		this.changeFocus(key, '#selections');	
    		return true;
    	}
    	return false;
    },
    
    // When "ENTER" is pressed on a link, or when link is clicked by mouse or touch screen
    click: function($jqobj) {
		var currLabel = this.data.label;
    	var linkid = $jqobj.attr('id');
        var parentid = $jqobj.parent().attr('id');
        var type = $jqobj.attr('data-type');
		var currlink = this.currsel;
		var keyname = $jqobj.attr('keyname');
		
		//var itemData = this._getMenuItemData($jqobj);
        	
    	if($jqobj.hasClass('back')) { // back button
    		this.destroy();
    		return true;
    	} 
    	
		if(linkid == 'exit') {			


			this.destroy();
            return false;
        }
        if(linkid =="meat") {
            this.$('#p1').attr('style','display:block');
            this.$('#p2').attr('style','display:none');
            this.$('#p3').attr('style','display:none');
            return true;
        }

        if(linkid =="breads") {
            this.$('#p1').attr('style','display:none');
            this.$('#p2').attr('style','display:block');
            this.$('#p3').attr('style','display:none');
            return true;
        }
        if(linkid =="cheese") {
            this.$('#p1').attr('style','display:none');
            this.$('#p2').attr('style','display:none');
            this.$('#p3').attr('style','display:block');
            return true;
        }

    	// page up/down
    	if( linkid == 'next' || linkid == 'prev') {
    	    var pos = this.$('#selections').scrollTop();
    	    var height = this.$('#selections').height();
    	    pos = (linkid=='next')? pos+height: pos-height;
    	    this.$('#selections').scrollTop(pos);
    	    return true;
    	}
		if(linkid == 'view') { // view order	
			if(window.meals.length>=1) {				
                var page = new dineview({viewId: 'dineview', breadcrumb:currLabel, data: ''});
                page.render();                
			} else {
            	var data = {
    	           text1: '<p>No Items Selected<br/><br/></p><p><span>Please make your selections<br/> and then view your order', 
    	       };
    	       var page = new Information({className:'dinedecline', data: data});
    	       page.render();
            }
			return true;
			}
            msg($jqobj.hasClass('item'));
            if($jqobj.hasClass('item')) { // order item			
                var currsel = this.$('#selection a.item.active');
                var currid = currsel.attr("data-index");
                var title = currsel.attr("title");
                var maxcarbs = this.maxcarbs;
                var carbs = currsel.attr("carbs");
                msg(carbs + ' ' + currid + ' ' + title );
                var totalcarbs = this.totalcarbs;
                var checkcarbs = Number(carbs) + Number(totalcarbs);
                msg(checkcarbs);
                if(checkcarbs > maxcarbs) {
					var data = {
						text1: '<p>Warning<br/><br/><br/><span>You are over your carbohydrate limit.<br/><br/>Please remove an item before<br/>adding another item.</p>'
					};
					var page = new Information({className:'maxselected', data: data});
					page.render();	
					return true;
				}

			this.coursemax = window.settings.coursemax;
			var coursemax = this.coursemax;
			var itemselected = currsel.hasClass('selected');
            msg(itemselected);
			if(itemselected!='')  {
				currsel.removeClass('selected')			
				this.selectedcount = this.selectedcount - 1;				
				var items = window.meals;  
				var i = 0;		
				for(var i=0; i<items.length; i++) {
					var item = items[i];
					var id = item["id"];
					msg('id ' + id + ' currid ' + currid);
					if(id==currid)
						window.meals.splice(i,1);
				}		
		
			 } else {
				
				this.selectedcount = this.selectedcount + 1;
				currsel.addClass('selected');	
				var item = [];
				item['id'] = currid;
				item['title'] = title;				
				item['qty'] = '1';
				item['carbs'] = carbs;
				item['max'] = coursemax;
				var len = window.meals.length;
				window.meals[len] = item;
				totalcarbs = totalcarbs + Number(carbs);				
				this.$('#nutrientstab #totalcarbs').html(totalcarbs);			
				this.totalcarbs = totalcarbs;
				window.settings.totalcarbs = totalcarbs;
				
			}
			this.$('#order').html("Order Item");
			
			
    	} 
    	
		if(linkid == 'help') {
    	       var data = {
    	           text1: '<p>Hungry?</p><p><span>Please press the red call <br/>button and a member of your <br/>Care Team will be by shortly</span></p>',    	         
    	       };
    	       var page = new Information({className:'dinedecline', data: data});
    	       page.render();				
				return true;
		}		
		    	
    	return false;
    },
    
    focus: function($jqobj) {
		this._super($jqobj);
    },
    
    blur: function($jqobj) {
    	this._super($jqobj);
    },
    
    renderData: function() {		
		this.selectedcount = 0;
        this._buildItems();
    },
    
    refresh: function() {
        this._buildItems();
    },
    
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
    
        /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/
    
		
	_buildItems: function () {
		
		var context = this;
        context.$('#selections').empty();
        var data = this.data;
		
		var meals = Array();
		var itemstring = '';
		var mealname = '';
		var maxcarbs = window.settings.maxcarbs;
		this.maxcarbs = maxcarbs;
		var totalcarbs = Number(window.settings.totalcarbs);
		
		this.totalcarbs = totalcarbs;
		this.$('#nutrientstab #totalcarbs').html(totalcarbs);			
		var pagebody = this.$('#selection #body').html('');		
		var pages = $('<div id="pages"></div>').appendTo(pagebody);
		var page1 = pages.append('<p id="p1" class="submenu active" style="display:block"></p>');
        
		var page2 = pages.append('<p id="p2" class="submenu" style="display:none"></p>');
		var page3 = pages.append('<p id="p3" class="submenu" style="display:none"></p>');
		
		var page = 1;
		

		if(maxcarbs>=1) {			
			this.$('#nutrientstab').attr('style','display:block');			
			this.$('#nutrientstab #max').html(maxcarbs);			
		} else 
			this.$('#nutrientstab').attr('style','display:none');		
          
		$(data).find("Item").each(function() { 

				var item = $(this).attr("TrayTicketLongName");
				var id = $(this).attr("Id");
				var keyname = $(this).attr("Keyname");
				var sortorder = $(this).attr("SecondarySort");
				var carbs = '';
				
				subdata = $(this);				
				if(maxcarbs>=1) {
					var carbs = 0;
					$(subdata).find("ItemNutrient").each(function() { 
						var nutrientid = $(this).attr("NutrientId");						
						if(nutrientid=='10101') {
							carbs = $(this).text();
						}
					});
				}
				
				var $entry = $('<a href="#" class="item"></a>').attr('data-index', id).attr('title', item)
					.attr('carbs',carbs).attr('keyname',keyname).text("___"+item);

				if(sortorder >= 0 && sortorder <= 99) 
					$entry.appendTo(context.$('#p1'));
				else if(sortorder >= 100 && sortorder <= 199) 
					$entry.appendTo(context.$('#p2'));
				else if(sortorder >= 200 && sortorder <= 299) 
					$entry.appendTo(context.$('#p3'));
				
				var items = window.meals;  
				var i = 0;		
				for(var i=0; i<items.length; i++) {
					var item = items[i];
					var itemid = item["id"];					
					if(itemid==id)
						$entry.addClass('selected');
		
				}		

			});
		
						             
    },
	
});    