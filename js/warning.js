var warning = View.extend({

    id: 'warning',
    
    template: 'warning.html',
    
    css: 'warning.css',
    
    trackPageView: false,
    
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/

    navigate: function(key)	{
    	if(key == 'LEFT' || key == 'RIGHT') {
    	    this.changeFocus(key, '#buttons', 'a.button');
    	    return true;
    	}
    	return false;
    },
    
    // When "ENTER" is pressed on a link, or when link is clicked by mouse or touch screen
    click: function($jqobj) {
    	var linkid = $jqobj.attr('id');
        
        if($jqobj.hasClass('back')) { // back button
    		this.destroy();
    		return true;
    	} 
   
		if(linkid == 'exit') {			
			window.meals =[];
		    keypressed('CLOSEALL');
            keypressed(216);    //force going back to main menu

			this.destroy();
            return false;
        }
   
    	return false;
    },
    
    renderData: function() {
        var context = this;
        var data = this.data;
        if(data.text1)
            this.$('#content #text1').html(data.text1);
        if(data.text2)
            this.$('#content #text2').html(data.text2);
        if(data.text3)
            this.$('#content #text3').html(data.text3);
            
        if(data.image1)
            this.$('#content #image1').html(data.image1);
        if(data.image2)
            this.$('#content #image2').html(data.image2);
        if(data.image3)
            this.$('#content #image3').html(data.image3);
    },
    
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
    
    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/
    
});    