var dinedeliverytime = View.extend({
    
    id: 'dinedeliverytime',
    
    template: 'dinedeliverytime.html',
    
    css: 'dinedeliverytime.css',
    
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/
    navigate: function(key)    {
        var navkeys = ['LEFT','RIGHT'];
        if(navkeys.indexOf(key)!=-1) {
            this.changeFocus(key);
            return true;
        }
        
        return false;
    },
    
    click: function($jqobj) {
		var linkid = $jqobj.attr('id');
		var currLabel = this.data.label
			
	    if(linkid == 'back' ) {
            this.destroy();
            return true;
        }
		if(linkid == 'order' ) {
            order_food();
            return true;
        }
		if($jqobj.hasClass('item')) {
			this.currsel = this.$('#selections a.active');
		}
        return false;
    },
    
    renderData: function() {
		var context = this;
		var data = this.data;
		// show the div
		var selections = this.$('#dinedeliverytime #selections');
		var deliverytimesstring = '';
			$(data).find("DeliveryTime").each(function() { 
				var time = $(this).text();
				var id = $(this).attr("Id");		
				var latestordertime = $(this).attr("LatestOrderTime");						
				latestordertime = latestordertime.replace("T"," ");
				var lastordertime = new Date(latestordertime);

				lastordertime = formatdateTime("yyyy-mm-dd HH:mm:ss",lastordertime);
				
				var now = formatdateTime("yyyy-mm-dd HH:mm:ss");
				datecompare = compareDates(latestordertime,now);								
				if(datecompare>=0) {
					deliverytimesstring = deliverytimesstring + '<a href="#" class = "time ' + time + '" id="' + id + '>' + time + '</a>';	
					var $entry = $('<a href="#" class="item ellipsis"></a>').attr('id', id).attr('title', time).text(time);
				
					$entry.appendTo(context.$('#selections'));
				}
				
				
			});
		
			context.$('#selections').html(deliverytimesstring);
		
	},
	
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
    order_food: function() { 
	        dataobj = {};
			dataobj['mrn'] = window.settings.mrn;
			dataobj['vendor'] = window.settings.vendor;
			dataobj['meal'] = window.diet.meal;
			
			var url = "http://10.54.10.104:8937/SubmitOrder"						
			var xml  = getXdXML(url, dataobj);      
			
			var FoodsResult = '';
			$(xml).find("GetFoodsResponse").each(function() {
				FoodsResult = $(xml).find("GetFoodsResult").text();			
			});

			var xmlObj = new DOMParser();   
			xmlObj = xmlObj.parseFromString(FoodsResult, "text/xml");     

	},
    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/

});

