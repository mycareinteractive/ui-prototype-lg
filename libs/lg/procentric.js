Procentric = {

  jsonpCallback: function(obj){
    alert("Object: " + obj.foo);
  },

    init: function(bootstrap){

        try
        {
            Debug.log('!! CHECKING FOR HCAP !!');
            hcap.property.getProperty({
                key: "model_name",
                onSuccess: function(e){
                    Debug.log('PLATFORM SUCCESS: ' + e.value);
                    $.cookie('platform', e.value);
                    platformversion = e.value;
                    version = "PROCENTRIC";
                    // window.settings.version = version;
                    $("#K_version").text(version);
                    hcap.network.getNetworkDevice({
                        "index":0,
                        "onSuccess":function(s) {
                            mac = s.mac;
                            hcap.network.getNetworkInformation({
                                onSuccess: function(s){
                                    //ip = s.ip_address;

                                    /*
                                    hcap.property.setProperty({
                                      "key":"tv_channel_attribute_floating_ui",
                                      "value":"0",
                                      "onSuccess":function() {

                                        console.log("set_no_floating_ui Success");
                                      },
                                      "onFailure":function(f) {
                                        console.log("set_no_floating_ui Failure : errorMessage = " + f.errorMessage);
                                      }
                                    });
                                    */

                                    bootstrap();
                                    // window["EOProcentric"] = this;
                                    // TEST JQUERY
                                    //jQuery.ajax({
                                    //  url: 'http://canceron.local:8080',
                                    //  dataType: 'jsonp',
                                    //  method: 'POST',
                                    //  data: { callback: "Procentric.jsonpCallback" },
                                    //  success: function(data){
                                    //  },
                                    //  error: function(qXHR, textStatus, errorThrown){
                                    //  }
                                    //});
                                    Debug.log("execute call to hcap.network.getNetworkInformation");
                                },
                                onFailure: function(f) {
                                    Debug.log("onFailure : errorMessage = " + f.errorMessage);
                                }
                            });
                        },
                        "onFailure":function(f) {
                            Debug.log("onFailure : errorMessage = " + f.errorMessage);
                        }
                    });
                },
                onFailure: function(f){
                    bootstrap();
                }
            });
        }
        catch(e)
        {
            Debug.log("!! FAILED TO EXECUTE HCAP !!");
        }

    },

    addCommandHandler: function(keyhandler){
        $(document).keydown(function(e){
            keyhandler(e);
        });
    },

    lgKeys: function(event)
    {
        Debug.log('lgKeys HAS BEEN EXECUTED!!');
        switch(event.keyCode)
        {
            case hcap.key.Code.EXIT:
                Debug.log('SETTING THE OSD TO OFF. MODE BEING SET TO: ' + hcap.mode.HCAP_MODE_0);

                try{
                    hcap.mode.setHcapMode({
                        mode: hcap.mode.HCAP_MODE_0,
                        onSuccess: function(){
                            Debug.log('Hiding the OSD');
                        },
                        onFailure: function(f){
                            Debug.log('We were not able to hide the OSD. Returned with error: \n' + f.errorMessage);
                        }
                    });

                    Debug.log('SETTING THE OSD TO OFF -- EXECUTED CALL');
                }
                catch(e){
                    Debug.log("FOR SOME REASON WE WERE NOT ABLE TO EXECUTE hcap.mode.setHcapMode");
                    Debug.log(e);
                }

                Debug.log('SETTING THE OSD TO OFF --- FINSISHED PROCESSING');
            break;

            case hcap.key.Code.PORTAL:
                Debug.log('SETTING THE OSD TO ON');
                hcap.mode.setHcapMode({
                    "mode": hcap.mode.HCAP_MODE_1,
                    "onSuccess": function(){
                        Debug.log('Hiding the TV');
                    },
                    "onFailure": function(f){
                        Debug.log('We were not able to hide the TV. Returned with error: \n' + f.errorMessage);
                    }
                });
            break;

            case hcap.key.Code.RED:
                location.href = "./init.php";
            break;

            case hcap.key.Code.GREEN:
                Debug.log('THE GREEN KEY HAS BEEN PRESSED');
                $('body').toggle();
            break;

            case hcap.key.Code.PLAY:
                hcap.Media.startUp({
                     "onSuccess":function() {
                         Debug.log("onSuccess");
                         var media = hcap.Media.createMedia({
                             "url":"http://portals.aceso.canceron.local/_videos/one_weekend_in_maine_2012_hd.mp4",
                             "mimeType":"video/mp4"
                         });
                         media.play({
                             "repeatCount":0,
                             "onSuccess":function() {
                                 Debug.log('We are playing the movie.');
                                 $('body').toggle();
                            },
                             "onFailure":function(f) {
                                 Debug.log("onFailure : errorMessage = " + f.errorMessage);
                             }
                         });

                         // media key event listeners.
                         $(document).keydown(function(e){
                            Debug.log("Caught a Key Code while a movie was playing!");
                            switch(e.keyCode)
                            {
                                case hcap.key.Code.PAUSE:
                                    Debug.log("Trying to stop the movie from playing.");
                                    media.stop({
                                        onSuccess: function(){
                                            Debug.log('WE STOPPED THE VIDEO!');
                                            media.destroy({
                                                 "onSuccess":function() {
                                                     Debug.log("onSuccess");
                                                     hcap.Media.shutDown({
                                                         "onSuccess":function() {
                                                             Debug.log("onSuccess");
                                                             $('body').toggle();
                                                         },
                                                         "onFailure":function(f) {
                                                             Debug.log("onFailure : errorMessage = " + f.errorMessage);
                                                         }
                                                     });
                                                 },
                                                 "onFailure":function(f) {
                                                     Debug.log("onFailure : errorMessage = " + f.errorMessage);
                                                 }
                                             });
                                        },
                                        onFailure: function(){
                                            Debug.log('WE could not STOPP THE VIDEO!');
                                        }
                                    });
                                    break;
                             }
                        });
                     },
                     "onFailure":function(f) {
                         Debug.log("onFailure : errorMessage = " + f.errorMessage);
                     }
                });
            break;
        }
    }

};