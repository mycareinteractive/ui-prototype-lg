/********************************************************************************
 * @brief																		*
 * 		Common JSON call functions			               						*
 *																				*
 * @author																		*
 *		Bill Sears\n															*
 *		Aceso\n																	*
 *		http://www.aceso.com\n													*
 *																				*
 * @modified																	*
 * 		Tami Seago, 02/05/2013, add info and error handling						*
 *																				*
 *		Bernie Zhao, 03/06/2013, added support for Live555 RTSP server			*
 *         replaced all absolute path config with relative path                 *
 ********************************************************************************/

function ewfObject()	{
    var client = 'KPNW';
    $("#K_client").text(client);
    
    // Portal config has been extracted to a seperated file portalconfig.js in the same folder as index.html'
    // By default the SVN package should include a portalconfig.template.js file.
    // When deploying copy this file and rename to portalconfig.js.
    // Developer should NOT check in the local portalconfig.js file
    return window.portalConfig;
}


function getRating(code)	{
	
	var rating  	= '';
	var ratingA 	= Array();
	
	ratingA['0'] = 'Not Rated';
	ratingA['1'] = 'G';
	ratingA['2'] = 'PG';
	ratingA['3'] = 'PG-13';
	ratingA['4'] = 'R';
	ratingA['5'] = 'NC-17';
	ratingA['6'] = 'Adult';
	ratingA['7'] = 'Unknown';
	ratingA['8'] = 'Unknown';
	ratingA['9'] = 'Unknown';
	
	rating = ratingA[code-1];
	
	return rating;
}

function getDevice() {
	
	var currentURL = location.href;
	var search = location.search;
	var version = $("#K_version").text();
	var mac = $("#K_mac").text();
	var ip = 'Unknown';
	var platformversion = 'Unknown';


    if ( typeof window['EONimbus'] != 'undefined' ) {           
        var device_list = Nimbus.getNetworkDeviceList();
        var name = device_list[0];
        var device = Nimbus.getNetworkDevice(name);
        mac = device.getMAC().replace( /:/gi, '' );
        ip = device.getIP() || 'Unknown';
        platformversion = Nimbus.getFirmwareRevision() || 'Unkown';
        version = 'ENSEO';      
    
    } 
    
    else if ( typeof window['EONebula'] != 'undefined' ) {          
        var device_list = Nebula.getNetworkDeviceList();
        var name = device_list[0];
        var device = Nebula.getNetworkDevice(name);
        mac = device.getMAC().replace( /:/gi, '' );
        if(device.getIP)
            ip = device.getIP() || 'Unknown';
        if(Nebula.getNativeRevision)
            platformversion = Nebula.getNativeRevision() || 'Unkown';
        version = 'NEBULA';     
        
    }
    
    else if (window.pluginObjectNetwork && window.pluginObjectNetwork.GetActiveType) {
        var connType = pluginObjectNetwork.GetActiveType();
        if(connType >= 0) {
            mac = pluginObjectNetwork.GetMAC(connType);
            ip = pluginObjectNetwork.GetIP(connType);
        }
        else {
            mac = pluginObjectNetwork.GetMAC(1);
        }
        
        if(webapis && webapis.tv && webapis.tv.info && webapis.tv.info.GetFirmware) {
            platformversion = webapis.tv.info.GetFirmware();
        }
        version = 'SAMSUNGTV';
    }
    else {      
        if(!version||!mac) {
            if ( currentURL.indexOf("https:\/\/") != -1 ) var obj = currentURL.replace("https:\/\/", "");
            else var obj = currentURL.replace("http:\/\/", "");
            obj=obj.split("\/");
            var currentDomain = obj[0];         
            var currentPortal = obj[1];         
            paramArray = obj[2].replace("index.html?","").split("\&");      
            var i;          
            for(var i = 0;i<paramArray.length;i++){
                if(unescape(paramArray[i].split("\=")[0])=="device-id")  mac = paramArray[i].split("\=")[1];
                if( unescape(paramArray[i].split("\=")[0])=="PollingIP")  var PollingIP = paramArray[i].split("\=")[1]||"";if( unescape(paramArray[i].split("\=")[0])=="PollingPort")  var PollingPort = paramArray[i].split("\=")[1]||"";
                if( unescape(paramArray[i].split("\=")[0])=="pageName")  var pageName = paramArray[i].split("\=")[1]||"";
                if( unescape(paramArray[i].split("\=")[0])=="assetLocalEntryUID")   var assetLocalEntryUID = paramArray[i].split("\=")[1]||"";
                if( unescape(paramArray[i].split("\=")[0])=="ticketID")  var ticketID = paramArray[i].split("\=")[1]||"";
                if( unescape(paramArray[i].split("\=")[0])=="guestName")  var guestName = paramArray[i].split("\=")[1]||"";             
            }               
            version = 'TCM';
        }
    }
	
	if (!mac||mac=='test')	{
		mac = "00045F905E45"
		version = 'TEST'
	} else if(mac=='test2') {
		mac = "00045F905DC4";				
		version = 'TEST';
	} 

	$("#K_mac").text(mac);
	$("#K_version").text(version);	
	$('#K_ip').text(ip);
	$('#K_platformversion').text(platformversion);
	window.settings.mac = mac;
	window.settings.version = version;
	window.settings.ip = ip;
	window.settings.platformVersion = platformversion;
	return mac;
}

function cleanJSON(data)	{
	
	data = clean2JSON(data);
	data = clean5JSON(data);

	return data;
}

function clean1JSON(data)	{

	check1 = data.indexOf('/');
	if (check1 != -1)	{
		while (data != (data = data.replace('/','&#47;')));
	}
	
	return data;
}

function clean2JSON(data)	{


	data = data.replace(/\\/g, '&#47;');    // convert all backslashes to slashes in SeaChange response
	data = data.replace(/&#47;"/g, '\\"'); // convert actual double quotation back
	
	return data;
}

function clean3JSON(data)	{

	check3 = data.indexOf(',http');
	if (check3 != -1)	{
		while (data != (data = data.replace(',http',' http')));
	}
	
	return data;
}

function clean4JSON(data)	{

	check4 = data.indexOf('http://192.168.200.60:9090/Poster/');
	if (check4 != -1)	{
		while (data != (data = data.replace('http://192.168.200.60:9090/Poster/','')));
	}	
	check4 = data.indexOf('http://192.168.100.60:9090/PICTURE/FOLDER/');
	if (check4 != -1)	{
		while (data != (data = data.replace('http://192.168.100.60:9090/PICTURE/FOLDER/','')));
		
	}	
	return data;
}

function clean5JSON(data)	{


	data = data.replace(/[\r\n]/g, '');
	return data;
}

function clean6JSON(data)	{

	while (data != (data = data.replace(null+',', '')));
	
	return data;
}


function ajaxJSON(url,args,ignore)	{
	


	try {
	var data = $.ajax({
		url: url,
		data: args,
		async: false,
		dataType: "json",
		cache: false
		}).responseText;
	
	msg('XHR finished loading: '+ url+'?'+args);
	
	if (ignore) 						{ msg('Ignoring JSON Returned Data: '+url+'?'+args); return; };
	if (!data) 							{ msg('FATAL ERROR!! JSON Data Not Found:  '+url+'?'+args+'  resulted in: NO DATA'); return; };
	if (data==''||data==' ') 			{ msg('FATAL ERROR!! JSON Data Incomplete: '+url+'?'+args+'  resulted in: '+data); return; };
	if (data.indexOf('{0}') != -1) 		{ msg('FATAL ERROR!! JSON Data Incomplete: '+url+'?'+args+'  resulted in: '+data); return; };
	if (data.indexOf('<?xml') != -1)	{ msg('FATAL ERROR!! JSON Data is XML: '+url+'?'+args+'  resulted in: '+data); return; };
	if (data.indexOf('<html>') != -1)	{ msg('FATAL ERROR!! JSON Data is HTML: '+url+'?'+args+'  resulted in: '+data); return; };	 
	if (data.indexOf('errorCode') != -1)	{} //msg('FATAL ERROR!! Error Code Received in JSON: '+url+'?'+args+'  resulted in: '+data);	 

	
	}
	catch (e) {
		msg(e.message);
		//gotomainmenu();		
	}
	
	if(url.indexOf('stbservlet')!=-1)   // clean up SeaChange JSON garbage
		data = cleanJSON(data);



	var jsondata = eval('(' + data + ')');
	
	return jsondata;
}

function loadEWF(type)	{

	var jsonString = $("#EWF_"+type).text();
	var jsonObject = JSON.parse(jsonString);

	return jsonObject;
}

function loadJSON(type)	{

	var jsonString = $("#JSON_"+type).text();
	if(!jsonString) {
	    msg('#JSON_' + type + ' is empty');
	    return null;
	}
	
	jsonString = cleanJSON(jsonString);	
	var jsonObject = JSON.parse(jsonString);

	return jsonObject;
}

function saveJSON(type,jsonObject)	{

	var jsonString = JSON.stringify(jsonObject);
	$("#X_"+type).text(jsonString);
	
	return true;
}

function getJSON(type)	{

	var jsonString = $("#X_"+type).text();
	jsonString = cleanJSON(jsonString);		
	var jsonObject = JSON.parse(jsonString);

	return jsonObject;
}
// CHANGE BEG: 20121024 sthummala - change via Tami - convert menu xml to js/json object
function getJSONMenu(type)	{
	var jsonString = $("#X_JSON_menu").text();
	jsonString = clean6JSON(jsonString);
	
	var jsonObject = JSON.parse(jsonString);
	return jsonObject;
}

function XML2jsobj(node) {

	var	data = {};

	// append a value
	function Add(name, value) {	
		if (data[name]) {
			if (data[name].constructor != Array) {
				data[name] = [data[name]];
			}
			data[name][data[name].length] = value;
		}
		else {
			data[name] = value;
		}
	};
	
	// element attributes
	var c, cn;
	/* Shirisha Thummala - Since no attributes commented the below part of the code */
	//for (c = 0; cn = node.attributes[c]; c++) {
	//	Add(cn.name, cn.value);
	//}
	
	// child elements
	for (c = 0; cn = node.childNodes[c]; c++) {
		if (cn.nodeType == 1) {
			if (cn.childNodes.length == 1 && cn.firstChild.nodeType == 3) {
				// text value
				Add(cn.nodeName, cn.firstChild.nodeValue);
			}
			else {
				// sub-object
				Add(cn.nodeName, XML2jsobj(cn));
			}
		}
	}


	return data;
	
}
// CHANGE END: 20121024 sthummala - change via Tami - convert menu xml to js/json object

if(window.Common && Common.API && Common.API.TVKeyValue) {
	window.tvKey = new Common.API.TVKeyValue();
}
else {
	window.tvKey = {};
}

function getkeys(keycode) {
	// notifications
    if(typeof keycode == 'string') {
        return keycode.toUpperCase();
    }
    
    // button commands
	var keys = Array();
	
	keys[13] = 'ENTER';
	
	keys[27] = 'VIDEO';    // ESC - video fullscreen toggle
	
	keys[33] = 'PGUP';
    keys[34] = 'PGDN';
	keys[35] = 'END';
	keys[36] = 'HOME';
	keys[37] = 'LEFT';
	keys[38] = 'UP';
	keys[39] = 'RIGHT';
	keys[40] = 'DOWN';
	

    //keys[46] = 'POWR';
    
	keys[48] = '0';
	keys[49] = '1';
	keys[50] = '2';
	keys[51] = '3';
	keys[52] = '4';
	keys[53] = '5';
	keys[54] = '6';
	keys[55] = '7';
	keys[56] = '8';
	keys[57] = '9';	
	
	keys[67] = 'CHDN';     // C key maps to channel-
	keys[88] = 'CHUP';     // X key maps to channel+

	keys[107] = 'VOLU';     // + on numpad
    keys[109] = 'VOLD';     // - on numpad
    
    keys[120] = 'RWND';     // F9
    keys[121] = 'PLAY';     // F10
    keys[122] = 'FFWD';     // F11
    keys[123] = 'STOP';     // F12
    
	keys[216] = 'MENU';
	
	// ******************************************************
    // Enseo codes
	keys[61441] = 'POWR';
	
    keys[61442] = 'UP';
	keys[61443] = 'DOWN';
	keys[61444] = 'LEFT';
	keys[61445] = 'RIGHT';
	keys[61446] = 'ENTER';
    
    keys[61447] = 'MENU';
	keys[61448] = 'VOLD';
	keys[61449] = 'VOLU';
	
	keys[61454] = '1';
    keys[61455] = '2';
    keys[61456] = '3';
    keys[61457] = '4';
    keys[61458] = '5';
    keys[61459] = '6';
    keys[61460] = '7';
    keys[61461] = '8';
    keys[61462] = '9';
    keys[61463] = '0'; 
	
    keys[61464] = 'PLAY';   // play/pause
    keys[61465] = 'STOP';   // stop
    keys[61465] = 'PAUS';   // pause
    keys[61467] = 'RWND';   // rewind
    keys[61468] = 'FFWD';   // fast forward
    keys[61483] = 'CHUP';   // channel+
    keys[61484] = 'CHDN';   // channel-
    keys[61507] = 'POWR';
    keys[61508] = 'OFF';
	keys[61521] = 'CC';		// closed caption
	
	// ******************************************************
    // Barco terminal codes
    keys[16777220] = 'ENTER';   // 0x01000004  RETURN
    keys[16777221] = 'ENTER';   // 0x01000005  ENTER
    keys[16777234] = 'LEFT';    // 0x01000012
    keys[16777235] = 'UP';      // 0x01000013
    keys[16777236] = 'RIGHT';   // 0x01000014
    keys[16777237] = 'DOWN';    // 0x01000015
    
    keys[16777328] = 'VOLD';    // Volume- on terminal panel maps to 0x01000070
    keys[16777329] = 'MUTE';    // Mute on Barco USB keyboard maps to 0x01000071
    keys[16777330] = 'VOLU';    // Volume+ on terminal panel maps to 0x01000072
    
    keys[16777345] = 'STOP';    // 0x01000081
    keys[16777346] = 'RWND';    // 0x01000082
    keys[16777347] = 'FFWD';    // 0x01000083
    keys[16777350] = 'PLAY';    // 0x01000086
    
    keys[16777399] = 'POWR';    // Power button on terminal panel maps to 0x010000b7
    
    keys[17825796] = 'CALL';    // Handset pick up event maps to 0x01100004
    keys[17825797] = 'HANG';    // Handset hang up event maps to 0x01100005
    
    keys[16777216] = 'VIDEO';   // ESC on terminal keyboard
    
    keys[16777272] = 'RWND';    // F9 on terminal keyboard
    keys[16777273] = 'PLAY';    // F10 on terminal keyboard
    keys[16777274] = 'FFWD';    // F11 on terminal keyboard
    keys[16777275] = 'STOP';    // F12 on terminal keyboard
    
    keys[16777459] = 'VIDEO';   // toggle fullscreen video player window 0x010000f3
    // ******************************************************
    // Samsung TV codes
    keys[tvKey.KEY_ENTER] = 'ENTER';
    keys[tvKey.KEY_RETURN] = 'CLOSE';
    keys[tvKey.KEY_EXIT] = 'EXIT';
    keys[tvKey.KEY_LEFT] = 'LEFT';
    keys[tvKey.KEY_UP] = 'UP';
    keys[tvKey.KEY_RIGHT] = 'RIGHT';
    keys[tvKey.KEY_DOWN] = 'DOWN';
    keys[tvKey.KEY_MENU] = 'MENU';
    
    keys[tvKey.KEY_PLAY] = 'PLAY';
    keys[tvKey.KEY_STOP] = 'STOP';
    keys[tvKey.KEY_PAUSE] = 'PAUS';
    keys[tvKey.KEY_RW] = 'RWND';
    keys[tvKey.KEY_FF] = 'FFWD';

    // ******************************************************
    // LG TV codes
    keys[hcap.key.Code.ENTER] = 'ENTER';
    keys[hcap.key.Code.LEFT] = 'LEFT';
    keys[hcap.key.Code.PAGE_UP] = 'PGUP';
    keys[hcap.key.Code.PAGE_DOWN] = 'PGDN';
    keys[hcap.key.Code.UP] = 'UP';
    keys[hcap.key.Code.RIGHT] = 'RIGHT';
    keys[hcap.key.Code.DOWN] = 'DOWN';
    keys[hcap.key.Code.BACK] = 'BACK';
    keys[hcap.key.Code.INFO] = 'INFO';
    keys[hcap.key.Code.RED] = 'RED';
    keys[hcap.key.Code.GREEN] = 'GREEN';
    keys[hcap.key.Code.YELLOW] = 'YELLOW';
    keys[hcap.key.Code.BLUE] = 'HOME';
    keys[hcap.key.Code.GUIDE] = 'GUIDE';
    keys[hcap.key.Code.EXIT] = 'EXIT';
    keys[hcap.key.Code.PORTAL] = 'PORTAL';
    keys[hcap.key.Code.PLAY] = 'PLAY';
    keys[hcap.key.Code.PAUSE] = 'PAUSE';
    keys[hcap.key.Code.CH_UP] = 'CHUP';
    keys[hcap.key.Code.CH_DOWN] = 'CHDN';
    
    keys[hcap.key.Code.NUM_0] = '0';
    keys[hcap.key.Code.NUM_1] = '1';
    keys[hcap.key.Code.NUM_2] = '2';
    keys[hcap.key.Code.NUM_3] = '3';
    keys[hcap.key.Code.NUM_4] = '4';
    keys[hcap.key.Code.NUM_5] = '5';
    keys[hcap.key.Code.NUM_6] = '6';
    keys[hcap.key.Code.NUM_7] = '7';
    keys[hcap.key.Code.NUM_8] = '8';
    keys[hcap.key.Code.NUM_9] = '9'; 
    
	
	// ******************************************************
	// special events
	keys['CLOSE'] = 'CLOSE';		
	keys['CLOSEALL'] = 'CLOSEALL';
	
	var key = keys[keycode];
	return key;

}
