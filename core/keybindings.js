Mousetrap.bind('0 1 3 5 enter', function(){
    hcap.power.reboot({
         "onSuccess":function() {
             console.log("******Reboot Success******");
         },
         "onFailure":function(f) {
             console.log("Reboot Failure : errorMessage = " + f.errorMessage);
         }
    });
});

Mousetrap.bind('0 2 4 6 enter', function(){
    location.href = location.href;
});