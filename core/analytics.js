(function(){
    var Tracker = Class.extend({
        
        provider: '',
        
        init: function() {
            var config = window.portalConfig;
            
            this.provider = config.analyticsProvider.toLowerCase();
            var url = config.analyticsUrl;
            var siteId = config.analyticsSite;
            
            switch(this.provider) {
                case 'google':
                    // load GA lib
                    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                    })(window,document,'script','//www.google-analytics.com/analytics.js','_ga');
                    
                    _ga('create', siteId, {
                        'cookieDomain': 'none',
                        'storage': 'none',
                        'clientId': window.settings.homeID
                    });
                    _ga('set', 'dimension1', window.settings.homeID);
                    break;
                    
                case 'piwik':
                    // load piwik lib
                    window._paq = window._paq || [];
                    (function() {
                        var u=url;
                        _paq.push(["setTrackerUrl", u+"piwik.php"]);
                        _paq.push(["setSiteId", siteId]);
                        _paq.push(['setSessionCookieTimeout', 14400]);
                        _paq.push(['disableCookies']);
                        var d=document, g=d.createElement("script"), s=d.getElementsByTagName("script")[0]; g.type="text/javascript";
                        g.defer=true; g.async=true; g.src=u+"piwik.js"; s.parentNode.insertBefore(g,s);
                    })();
                    
                    break;
            }
        },
        
        pageView: function(url, title, options) {
            if(this.provider == 'google') {
                var options = options || {};
                if(url)
                    options['page'] = url;
                if(title)
                    options['title'] = title;
                _ga('send', 'pageview', options);
            }
            else if(this.provider == 'piwik') {
                if(url)
                    _paq.push(['setCustomUrl', url]);
                if(title)
                    _paq.push(['setDocumentTitle', title]);
                _paq.push(['trackPageView']);
            }
        },
        
        customDimension: function(index, name, value, scope) {
            if(this.provider == 'google') {
                _ga('set', 'dimension'+index, value);
            }
            else if(this.provider == 'piwik') {
                _paq.push(['setCustomVariable', index, name, value, scope]); 
            }
        },
        
        event: function(category, action, label, value, options) {
            // We now simulate event by pageviews on all analytics engines
            if(this.provider == 'google') {
                //_ga('send', 'event', category, action, label, value, options);
                var options = options || {};
                if(action)
                    options['page'] = '/event/' + category + '/' + action;
                if(label)
                    options['title'] = label;
                _ga('send', 'pageview', options);
            }
            else if(this.provider == 'piwik') {
                if(action)
                    _paq.push(['setCustomUrl', '/event/' + category + '/' + action]);
                if(label)
                    _paq.push(['setDocumentTitle', label]);
                _paq.push(['trackPageView']); 
            }
        }
    });

    window.nova = window.nova || {};    
    window.nova.tracker = window.nova.tracker || new Tracker(); 
})();



