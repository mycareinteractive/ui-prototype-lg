Debug = {

    trace: function (msg) {
      jQuery("#dbgOut").append("<p>" + msg + "</p>");
    },

    log: function (msg) {
      console.log("\n#########################\n\n" + msg + "\n\n#########################");
      return true;
    }

};